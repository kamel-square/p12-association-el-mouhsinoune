import { Component, OnInit } from '@angular/core';
import {AdherentService} from "../services/adherent.service";
import {JwtClientService} from "../services/jwt-client.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-membre-famille',
  templateUrl: './add-membre-famille.component.html',
  styleUrls: ['./add-membre-famille.component.css']
})
export class AddMembreFamilleComponent implements OnInit {

  adherent;
  lienParentes;

  constructor(private adherentService: AdherentService,
              public jwtService: JwtClientService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['adherentId'];
    this.adherentService.getAdherentById(+id).subscribe(data => {
      this.adherent = data;
    }, error => {
      console.log(error);
    });

    this.adherentService.getLienParentes().subscribe(data => {
      this.lienParentes = data;
    });
  }

  onSaveMembreFamille(value) {
    for (let lp of this.lienParentes) {
      if (lp.lien == value.lienParenteId) {
        value.lienParenteId = lp.id;
        this.adherentService.saveMembreFamille(value).subscribe(data => {
          if (this.jwtService.utilisateur.id == this.adherent.utilisateurId) {
            this.router.navigate(['/monCompte/' + this.adherent.id]);
          } else {
            this.router.navigate(['/infosAdherent/' + this.adherent.id]);
          }
        });
      }
    }
  }
}
