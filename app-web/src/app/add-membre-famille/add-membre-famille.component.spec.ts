import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMembreFamilleComponent } from './add-membre-famille.component';

describe('AddMembreFamilleComponent', () => {
  let component: AddMembreFamilleComponent;
  let fixture: ComponentFixture<AddMembreFamilleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddMembreFamilleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMembreFamilleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
