import { Component, OnInit } from '@angular/core';
import {EvenementService} from "../services/evenement.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AdherentService} from "../services/adherent.service";

@Component({
  selector: 'app-update-evenement',
  templateUrl: './update-evenement.component.html',
  styleUrls: ['./update-evenement.component.css']
})
export class UpdateEvenementComponent implements OnInit {

  evenement;
  adherent;
  natureEvenements;
  currentNomAdherent;

  constructor(private evenementService: EvenementService,
              private route: ActivatedRoute,
              private adherentService: AdherentService,
              private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.evenementService.getEvenementById(+id).subscribe(data => {
      this.evenement = data;
      console.log(this.evenement);
      this.getAdherent(this.evenement.adherentId);
    }, error => {
      console.log(error);
    });

    this.evenementService.getNatureEvenements().subscribe(data => {
      this.natureEvenements = data;
    }, error => {
      console.log(error);
    });
  }

  getAdherent(id) {
    this.adherentService.getAdherentById(id).subscribe(data => {
      this.adherent = data;
      this.currentNomAdherent = this.adherent.nom;
    }, error => {
      console.log(error);
    });
  }

  getNatureEvenement(value) {
    this.evenementService.getNatureEvenements().subscribe(data => {
      this.natureEvenements = data;
      console.log(this.natureEvenements);
      for (let e of this.natureEvenements) {
        if (e.natureEvenement == value.natureEvenementId) {
          value.natureEvenement = e;
          value.natureEvenementId = e.id;
          this.updateEvenement(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  updateEvenement(value) {
    this.evenementService.updateEvenement(value).subscribe(data => {
      this.evenement = data;
      console.log(this.evenement);
    }, error => {
      console.log(error);
    });
  }

  onSave(value) {
    this.getNatureEvenement(value);
    console.log("Nature = " + value.natureEvenementId);
    this.router.navigate(['evenement'])
      .then(() => {
        window.location.reload();
      });
  }
}
