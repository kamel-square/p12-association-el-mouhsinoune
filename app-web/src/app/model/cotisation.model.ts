export class Cotisation {

  public id: number;
  public adherentId: number;
  public montant: number;
  public date: Date;
  public periode: number;
  public moyenPaiementId: number;

  public nomAdherent: string;
}
