export class Adherent {

  public id: number;
  public numAdherent: string;
  public nom: string;
  public prenom: string;
  public email: string;
  public adresse: string;
  public situation: string;
  public telephone: string;
  public paysId: number;
  public utilisateurId: number;
  public genreId: number;

  public pays: string;
}
