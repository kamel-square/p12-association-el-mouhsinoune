import {Component, OnInit} from '@angular/core';
import {CotisationService} from "../services/cotisation.service";
import {AdherentService} from "../services/adherent.service";
import {EvenementService} from "../services/evenement.service";
import {Adherent} from "../model/adherent.model";
import {JwtClientService} from "../services/jwt-client.service";

@Component({
  selector: 'app-cotisations',
  templateUrl: './cotisations.component.html',
  styleUrls: ['./cotisations.component.css']
})
export class CotisationsComponent implements OnInit {

  adherents;
  cotisations;
  moyenPaiement;
  adherentList;
  public size: number = 5;
  public currentPage: number = 0;
  public totalPages: number;
  public pages: Array<number>;
  public currentDate: string = "";
  public currentAdherentId: number = 0;
  public currentMoyenPaiementId: number = 0;
  searchAdherent: Adherent[];
  chaine = "";
  moyens;
  private adherentsByNom;
  utilisateur;
  adherentNomAndPrenom;

  constructor(private cotisationService: CotisationService,
              private adherentService: AdherentService,
              private evenementService: EvenementService,
              private jwtService: JwtClientService) {
  }

  ngOnInit(): void {
    this.utilisateur = this.jwtService.utilisateur;
    this.searchAdherent = [];
    this.adherentService.getAdherents().subscribe(data => {
      this.adherentList = data;
    }, error => {
      console.log(error);
    });

    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyens = data;
    }, error => {
      console.log(error);
    });
    this.searchCotisation();
  }

  onSearch(value) {
    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyens = data;

      for (let m of this.moyens) {
        if (m.moyenPaiement == value.moyenPaiementId) {
          value.moyenPaiementId = m.id;
          this.currentMoyenPaiementId = m.id;

          if (this.currentMoyenPaiementId == null || value.moyenPaiementId == 0) {
            this.currentMoyenPaiementId = 0;
          }
        }
      }

      this.currentPage = 0;
      this.currentDate = value.date;

      if (this.currentDate == undefined) {
        this.currentDate = "";
      }

      this.searchAdherent = [];
      this.search(value);
      this.chaine = "";

    }, error => {
      console.log(error);
    });

  }

  private searchCotisation() {
    this.cotisationService.getCotisationsByKeyword(this.currentDate, this.currentAdherentId, this.currentMoyenPaiementId,
      this.currentPage, this.size).subscribe(data => {
      this.totalPages = data["totalPages"];
      this.pages = new Array<number>(this.totalPages);
      this.cotisations = data["content"];
      for (let c of this.cotisations) {
        this.evenementService.getMoyenPaiementById(c.moyenPaiementId).subscribe(data => {
          this.moyenPaiement = data;
          if (c.moyenPaiementId == this.moyenPaiement.id) {
            c.moyenPaiementId = this.moyenPaiement.moyenPaiement;
            this.currentMoyenPaiementId = 0;
          }
        }, error => {
          console.log(error);
        });
        this.adherentService.getAdherentById(c.adherentId).subscribe(data => {
          this.adherents = data;
          if (c.adherentId == this.adherents.id) {
            c.nomAdherent = this.adherents.nom + " " + this.adherents.prenom
          }
        }, error => {
          console.log(error);
        });

      }
    }, error => {
      console.log(error);
    });

  }

  onDeleteCotisation(c) {
    let confirmation = confirm("Êtes-vous sûr de vouloir la supprimer ?");
    if (confirmation) {
      this.cotisationService.deleteCotisation(c.id)
        .subscribe(data => {
          this.searchCotisation();
        }, error => {
          console.log(error);
        });
    }
  }

  onPageCotisation(i: number) {
    this.currentPage = i;
    this.searchCotisation();
  }

  search(value) {
    const nom = this.chaine.split(' ');
    if (this.chaine != "") {
      this.adherentService.getAdherentByNom(nom[0]).subscribe((adherent) => {
        this.adherentsByNom = adherent;
        this.searchAdherent = this.adherentsByNom;
        for (let a of this.adherentsByNom) {
          this.adherentNomAndPrenom = a.nom + " " + a.prenom;
          if (value.adherentId == a.nom + " " + a.prenom) {
            this.currentAdherentId = a.id;
          }
        }
        this.searchCotisation();
      });
    } else {
      this.searchAdherent = [];
      if (value.adherentId == "") {
        this.currentAdherentId = 0;
      }
      this.searchCotisation();
    }
  }
}
