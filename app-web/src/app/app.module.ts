import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { AdherentsComponent } from './adherents/adherents.component';
import { EvenementComponent } from './evenement/evenement.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AddAdherentComponent } from './add-adherent/add-adherent.component';
import { AddEvenementComponent } from './add-evenement/add-evenement.component';
import { UpdateAdherentComponent } from './update-adherent/update-adherent.component';
import { InfosAdherentComponent } from './infos-adherent/infos-adherent.component';
import { UpdateEvenementComponent } from './update-evenement/update-evenement.component';
import { AddCotisationComponent } from './add-cotisation/add-cotisation.component';
import { InfosEvenementComponent } from './infos-evenement/infos-evenement.component';
import { AddCoutComponent } from './add-cout/add-cout.component';
import { CotisationsComponent } from './cotisations/cotisations.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AngularMaterialModule} from "./angular-material.module";
import { UpdateCotisationComponent } from './update-cotisation/update-cotisation.component';
import {PdfViewerModule} from "ng2-pdf-viewer";
import { AuthComponent } from './auth/auth.component';
import { MonCompteComponent } from './mon-compte/mon-compte.component';
import { AddDocumentComponent } from './add-document/add-document.component';
import { AddMembreFamilleComponent } from './add-membre-famille/add-membre-famille.component';
import { AccueilComponent } from './accueil/accueil.component';
import { UpdateCoutComponent } from './update-cout/update-cout.component';
import { UpdateDocumentComponent } from './update-document/update-document.component';
import { UpdateMembreFamilleComponent } from './update-membre-famille/update-membre-famille.component';

@NgModule({
  declarations: [
    AppComponent,
    AdherentsComponent,
    EvenementComponent,
    AddAdherentComponent,
    AddEvenementComponent,
    UpdateAdherentComponent,
    InfosAdherentComponent,
    UpdateEvenementComponent,
    AddCotisationComponent,
    InfosEvenementComponent,
    AddCoutComponent,
    CotisationsComponent,
    UpdateCotisationComponent,
    AuthComponent,
    MonCompteComponent,
    AddDocumentComponent,
    AddMembreFamilleComponent,
    AccueilComponent,
    UpdateCoutComponent,
    UpdateDocumentComponent,
    UpdateMembreFamilleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    PdfViewerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
