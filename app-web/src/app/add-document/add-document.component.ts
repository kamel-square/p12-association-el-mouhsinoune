import {Component, OnInit} from '@angular/core';
import {Adherent} from "../model/adherent.model";
import {EvenementService} from "../services/evenement.service";
import {AdherentService} from "../services/adherent.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpEventType, HttpResponse} from "@angular/common/http";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.css']
})
export class AddDocumentComponent implements OnInit {

  evenement;
  list;
  chaine;
  natureDocuments;
  currentDocument;
  documentAdded: boolean;
  dateNow = Date.now();
  pipe = new DatePipe('en-US');
  date = new Date().toLocaleDateString();
  dateForDb;
  private progress: number;
  selectedFiles;
  private currentFileUpload;

  constructor(private evenementService: EvenementService,
              private adherentService: AdherentService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const evenementId = this.route.snapshot.params['evenementId'];
    this.evenementService.getEvenementById(+evenementId).subscribe(data => {
      this.evenement = data;
    }, error => {
      console.log(error);
    });

    this.dateForDb = this.pipe.transform(this.dateNow, 'yyyy-MM-dd');

    this.evenementService.getNatureDocuments().subscribe(data => {
      this.natureDocuments = data;
    }, error => {
      console.log(error);
    });
  }

  save(value) {
    this.evenementService.getNatureDocuments().subscribe(data => {
      this.list = data;
      for (let d of this.list) {
        if (d.natureDocument == value.natureDocumentId) {
          value.natureDocumentId = d.id;
          this.addDocument(value);
        }
      }
    }, error => {
      console.log(error);
    })
  }

  onSelectedFile(event) {
    this.selectedFiles = event.target.files;
  }

  uploadDocument(value) {
    this.progress = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.evenementService.uploadFile(this.currentFileUpload, value).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        alert("Document enregistré avec succès");
        this.documentAdded = false;
        this.router.navigate(['/infosEvenement/' + this.evenement.id]);
      }
    }, error => {
      console.log(error);
      alert("Problème de chargement");
    });
  }

  onSaveDocument(value) {
    this.save(value);
  }

  addDocument(value) {
    value.document = ' ';
    value.date = this.dateForDb;
    this.evenementService.addDocument(value).subscribe(data => {
      this.documentAdded = true;
      this.currentDocument = data;
    },error => {
      console.log(error);
    });
  }
}
