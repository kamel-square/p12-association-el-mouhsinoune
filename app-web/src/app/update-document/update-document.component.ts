import { Component, OnInit } from '@angular/core';
import {EvenementService} from "../services/evenement.service";
import {AdherentService} from "../services/adherent.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpEventType, HttpResponse} from "@angular/common/http";

@Component({
  selector: 'app-update-document',
  templateUrl: './update-document.component.html',
  styleUrls: ['./update-document.component.css']
})
export class UpdateDocumentComponent implements OnInit {

  document;
  adherent;
  currentNatureDocument;
  natureDocuments;
  selectedFiles;
  evenement;
  nature;
  progress;
  currentFileUpload;

  constructor(private evenementService: EvenementService,
              private adherentService: AdherentService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.evenementService.getDocumentById(+id).subscribe(data => {
      this.document = data;
      this.getEvenement(this.document.evenementId);
      this.getNatureDocumentById(this.document.natureDocumentId);
    }, error => {
      console.log(error);
    });

    this.evenementService.getNatureDocuments().subscribe(data => {
      this.natureDocuments = data;
    }, error => {
      console.log(error);
    });
  }

  getEvenement(id) {
    this.evenementService.getEvenementById(id).subscribe(data => {
      this.evenement = data;
      this.getAdherent(this.evenement.adherentId);
    }, error => {
      console.log(error);
    });
  }

  getAdherent(id) {
    this.adherentService.getAdherentById(id).subscribe(data => {
      this.adherent = data;
    }, error => {
      console.log(error);
    });
  }

  getNatureDocumentById(id) {
    this.evenementService.getNatureDocumentById(id).subscribe(data => {
      this.nature = data;
      this.currentNatureDocument = this.nature.natureDocument;
    }, error => {
      console.log(error);
    });
  }

  onSave(value) {
    if (this.selectedFiles != undefined) {
      this.uploadDocument(value);
    } else {
      value.document = this.document.document;
    }
    this.getNatureDocument(value);
    this.router.navigate(['infosEvenement/' + this.document.evenementId])
      .then(() => {
        window.location.reload();
      });
  }

  getNatureDocument(value) {
    this.evenementService.getNatureDocuments().subscribe(data => {
      this.natureDocuments = data;
      for (let nd of this.natureDocuments) {
        if (nd.natureDocument == value.natureDocumentId) {
          value.natureDocumentId = nd.id;
          this.updateDocument(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  onSelectedFile(event) {
    this.selectedFiles = event.target.files;
  }

  uploadDocument(value) {
    this.progress = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.evenementService.uploadFile(this.currentFileUpload, value.id).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        alert("Document enregistré avec succès");
        this.router.navigate(['/infosEvenement/' + this.evenement.id]);
      }
    }, error => {
      console.log(error);
      alert("Problème de chargement");
    });
  }

  private updateDocument(value) {
    this.evenementService.updateDocument(value).subscribe(data => {
      this.document = data;
    });
  }
}
