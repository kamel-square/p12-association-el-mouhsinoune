import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateCoutComponent } from './update-cout.component';

describe('UpdateCoutComponent', () => {
  let component: UpdateCoutComponent;
  let fixture: ComponentFixture<UpdateCoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateCoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateCoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
