import { Component, OnInit } from '@angular/core';
import {EvenementService} from "../services/evenement.service";
import {AdherentService} from "../services/adherent.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-update-cout',
  templateUrl: './update-cout.component.html',
  styleUrls: ['./update-cout.component.css']
})
export class UpdateCoutComponent implements OnInit {
  cout;
  adherent;
  evenement;
  moyenPaiements;
  currentMoyenPaiement;
  currentNomAdherent;
  moyen;
  natureCouts;
  nature;
  currentNatureCout;

  constructor(private evenementService: EvenementService,
              private adherentService: AdherentService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.evenementService.getCoutById(+id).subscribe(data => {
      this.cout = data;
      this.getEvenement(this.cout.evenementId);
      this.getMoyenPaiementById(this.cout.moyenPaiementId);
      this.getNatureCoutById(this.cout.natureCoutId);
    }, error => {
      console.log(error);
    });

    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
    }, error => {
      console.log(error);
    });

    this.evenementService.getNatureCout().subscribe(data => {
      this.natureCouts = data;
    }, error => {
      console.log(error);
    });
  }

  getEvenement(id) {
    this.evenementService.getEvenementById(id).subscribe(data => {
      this.evenement = data;
      this.getAdherent(this.evenement.adherentId);
    }, error => {
      console.log(error);
    });
  }

  getAdherent(id) {
    this.adherentService.getAdherentById(id).subscribe(data => {
      this.adherent = data;
      this.currentNomAdherent = this.adherent.nom;
    }, error => {
      console.log(error);
    });
  }

  getNatureCoutById(id) {
    this.evenementService.getNatureCoutById(id).subscribe(data => {
      this.nature = data;
      this.currentNatureCout = this.nature.natureCout;
    }, error => {
      console.log(error);
    });
  }

  getMoyenPaiementById(id) {
    this.evenementService.getMoyenPaiementById(id).subscribe(data => {
      this.moyen = data;
      this.currentMoyenPaiement = this.moyen.moyenPaiement;
    }, error => {
      console.log(error);
    });
  }

  onSave(value) {
    this.getMoyenPaiements(value);
    this.router.navigate(['infosEvenement/' + this.cout.evenementId])
      .then(() => {
        window.location.reload();
      });
  }

  getNatureCouts(value) {
    this.evenementService.getNatureCout().subscribe(data => {
      this.natureCouts = data;
      for (let nc of this.natureCouts) {
        if (nc.natureCout == value.natureCoutId) {
          value.natureCoutId = nc.id;
          this.updateCout(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  getMoyenPaiements(value) {
    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
      for (let mp of this.moyenPaiements) {
        if (mp.moyenPaiement == value.moyenPaiementId) {
          value.moyenPaiementId = mp.id;
          this.getNatureCouts(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  updateCout(value) {
    this.evenementService.updateCout(value).subscribe(data => {
      this.cout = data;
    });
  }
}
