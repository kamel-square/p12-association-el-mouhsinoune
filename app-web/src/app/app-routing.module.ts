import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdherentsComponent} from "./adherents/adherents.component";
import {EvenementComponent} from "./evenement/evenement.component";
import {AddAdherentComponent} from "./add-adherent/add-adherent.component";
import {AddEvenementComponent} from "./add-evenement/add-evenement.component";
import {UpdateAdherentComponent} from "./update-adherent/update-adherent.component";
import {InfosAdherentComponent} from "./infos-adherent/infos-adherent.component";
import {UpdateEvenementComponent} from "./update-evenement/update-evenement.component";
import {AddCotisationComponent} from "./add-cotisation/add-cotisation.component";
import {InfosEvenementComponent} from "./infos-evenement/infos-evenement.component";
import {AddCoutComponent} from "./add-cout/add-cout.component";
import {CotisationsComponent} from "./cotisations/cotisations.component";
import {UpdateCotisationComponent} from "./update-cotisation/update-cotisation.component";
import {AuthComponent} from "./auth/auth.component";
import {JwtClientService} from "./services/jwt-client.service";
import {MonCompteComponent} from "./mon-compte/mon-compte.component";
import {AddDocumentComponent} from "./add-document/add-document.component";
import {AddMembreFamilleComponent} from "./add-membre-famille/add-membre-famille.component";
import {AccueilComponent} from "./accueil/accueil.component";
import {UpdateCoutComponent} from "./update-cout/update-cout.component";
import {UpdateDocumentComponent} from "./update-document/update-document.component";
import {UpdateMembreFamilleComponent} from "./update-membre-famille/update-membre-famille.component";

const routes: Routes = [
  {
    path: 'accueil', component: AccueilComponent
  },
  {
    path: 'adherents', canActivate: [JwtClientService], component: AdherentsComponent
  },
  {
    path: 'addAdherent', component: AddAdherentComponent
  },
  {
    path: 'addMembreFamille/:adherentId', canActivate: [JwtClientService], component: AddMembreFamilleComponent
  },
  {
    path: 'evenement', canActivate: [JwtClientService], component: EvenementComponent
  },
  {
    path: 'cotisations', canActivate: [JwtClientService], component: CotisationsComponent
  },
  {
    path: 'addEvenement', canActivate: [JwtClientService], component: AddEvenementComponent
  },
  {
    path: 'updateAdherent/:id', canActivate: [JwtClientService], component: UpdateAdherentComponent
  },
  {
    path: 'infosAdherent/:id', canActivate: [JwtClientService], component: InfosAdherentComponent
  },
  {
    path: 'monCompte/:utilisateurId', canActivate: [JwtClientService], component: MonCompteComponent
  },
  {
    path: 'infosEvenement/:id', canActivate: [JwtClientService], component: InfosEvenementComponent
  },
  {
    path: 'updateEvenement/:id', canActivate: [JwtClientService], component: UpdateEvenementComponent
  },
  {
    path: 'addCotisation', canActivate: [JwtClientService], component: AddCotisationComponent
  },
  {
    path: 'updateCotisation/:id', canActivate: [JwtClientService], component: UpdateCotisationComponent
  },
  {
    path: 'addCout/:evenementId', canActivate: [JwtClientService], component: AddCoutComponent
  },
  {
    path: 'addDocument/:evenementId', canActivate: [JwtClientService], component: AddDocumentComponent
  },
  {
    path: 'updateCout/:id', canActivate: [JwtClientService], component: UpdateCoutComponent
  },
  {
    path: 'updateDocument/:id', canActivate: [JwtClientService], component: UpdateDocumentComponent
  },
  {
    path: 'updateMembreFamille/:id', canActivate: [JwtClientService], component: UpdateMembreFamilleComponent
  },
  {
    path: 'auth', component: AuthComponent
  },
  {
    path: '', redirectTo: 'accueil', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
