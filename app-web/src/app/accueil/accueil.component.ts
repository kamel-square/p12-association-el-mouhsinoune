import { Component, OnInit } from '@angular/core';
import {JwtClientService} from "../services/jwt-client.service";

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {

  constructor(public jwtService: JwtClientService) { }

  ngOnInit(): void {
  }

}
