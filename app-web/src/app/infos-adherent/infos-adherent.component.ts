import { Component, OnInit } from '@angular/core';
import {AdherentService} from "../services/adherent.service";
import {ActivatedRoute, Router} from "@angular/router";
import {EvenementService} from "../services/evenement.service";
import {CotisationService} from "../services/cotisation.service";
import {JwtClientService} from "../services/jwt-client.service";

@Component({
  selector: 'app-infos-adherent',
  templateUrl: './infos-adherent.component.html',
  styleUrls: ['./infos-adherent.component.css']
})
export class InfosAdherentComponent implements OnInit {

  adherent;
  utilisateur;
  evenements;
  cotisations;
  sizeList;
  moyenPaiements;
  membreFamilles;

  constructor(private adherentService: AdherentService,
              private route: ActivatedRoute,
              private evenementService: EvenementService,
              private cotisationService: CotisationService,
              private jwtService: JwtClientService,
              private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.adherentService.getAdherentById(+id).subscribe(data => {
      this.adherent = data;
    }, error => {
      console.log(error);
    });
    this.utilisateur = this.jwtService.utilisateur;
    this.getEvenement(+id);
    this.getCotisationsByadherentId(+id);
    this.getMembreFamilleByAdherentId(+id);
  }

  getEvenement(adherentId) {
    this.evenementService.getEvenementsByAdherentId(adherentId).subscribe(data => {
      this.evenements = data;
      this.sizeList = this.evenements.length;
    }, error => {
      console.log(error);
    });
  }

  getCotisationsByadherentId(adherentId) {
    this.cotisationService.getCotisationsByAdherentId(adherentId).subscribe(data => {
      this.cotisations = data;
      for (let c of this.cotisations) {
        this.evenementService.getMoyenPaiementById(c.moyenPaiementId).subscribe(data => {
          this.moyenPaiements = data;
          c.moyenPaiementId = this.moyenPaiements.moyenPaiement;
        }, error => {
          console.log(error);
        });
      }
    }, error => {
      console.log(error);
    });
  }

  getMoyenPaiementById(id) {
    this.evenementService.getMoyenPaiementById(id).subscribe(data => {
      this.moyenPaiements = data;
    }, error => {
      console.log(error);
    });
  }

  getMembreFamilleByAdherentId(adherentId) {
    this.adherentService.getMembreFamillesByAdherentId(adherentId).subscribe(data => {
      this.membreFamilles = data;
    }, error => {
      console.log(error);
    });
  }

  getMoyenPaiement() {
    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
      return data;
    }, error => {
      console.log(error);
    });
  }

  onDeleteMembreFamille(mf) {
    let confirmation = confirm("Êtes-vous sûr de vouloir le supprimer ?");
    if (confirmation) {
      this.adherentService.deleteMembreFamille(mf.id)
        .subscribe(data => {
          location.reload();
        }, error => {
          console.log(error);
        });
    }
  }
}
