import { Component, OnInit } from '@angular/core';
import {EvenementService} from "../services/evenement.service";
import {Router} from "@angular/router";
import {AdherentService} from "../services/adherent.service";
import {Adherent} from "../model/adherent.model";

@Component({
  selector: 'app-add-evenement',
  templateUrl: './add-evenement.component.html',
  styleUrls: ['./add-evenement.component.css']
})
export class AddEvenementComponent implements OnInit {

  evenements;
  list;
  chaine;
  natureEvenements;
  adherentsByNom;
  searchAdherent: Adherent[];
  adherentNomAndPrenom;

  constructor(private evenementService: EvenementService,
              private adherentService: AdherentService,
              private router: Router) { }

  ngOnInit(): void {
    this.evenementService.getNatureEvenements().subscribe(data => {
      this.natureEvenements = data;
    }, error => {
      console.log(error);
    });

  }

  save(value) {
    this.evenementService.getNatureEvenements().subscribe(data => {
      this.list = data;
      for (let e of this.list) {
        if (e.natureEvenement == value.natureEvenementId) {
          value.natureEvenementId = e.id;
          this.search(value);
        }
      }
    }, error => {
      console.log(error);
    })
  }

  addEvenement(value) {
    this.evenementService.addEvenement(value).subscribe(data => {
      this.adherentNomAndPrenom = '';
      this.router.navigate(['/evenement']);
    }, error => {
      console.log(error);
    });
  }

  onSaveEvenement(value) {
    this.save(value);
  }

  search(value) {
    const nom = this.chaine.split(' ');
    if (nom.length) {
      this.adherentService.getAdherentByNom(nom[0]).subscribe((adherent) => {
        this.adherentsByNom = adherent;
        this.searchAdherent = this.adherentsByNom;
        for (let a of this.adherentsByNom) {
          this.adherentNomAndPrenom = a.nom + " " + a.prenom;
          if (value.adherentId == a.nom + " " + a.prenom) {
            value.adherentId = a.id;
            this.addEvenement(value);
          }
        }
      });
    } else {
      this.searchAdherent = [];
    }
  }
}
