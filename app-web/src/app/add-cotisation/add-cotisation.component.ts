import { Component, OnInit } from '@angular/core';
import {CotisationService} from "../services/cotisation.service";
import {Router} from "@angular/router";
import {AdherentService} from "../services/adherent.service";
import {EvenementService} from "../services/evenement.service";

@Component({
  selector: 'app-add-cotisation',
  templateUrl: './add-cotisation.component.html',
  styleUrls: ['./add-cotisation.component.css']
})
export class AddCotisationComponent implements OnInit {

  cotisations;
  adherents;
  moyenPaiements;
  chaine;
  adherentsByNom;
  searchAdherent;
  currentAdherentId;
  adherentNomAndPrenom;

  constructor(private cotisationService: CotisationService,
              private adherentService: AdherentService,
              private evenementService: EvenementService,
              private router: Router) { }

  ngOnInit(): void {
    this.adherentService.getAdherents().subscribe(data => {
      this.adherents = data;
    }, error => {
      console.log(error);
    });

    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
    }, error => {
      console.log(error);
    });
  }

  getMoyenPaiements(value) {
    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
      for (let mp of this.moyenPaiements) {
        if (value.moyenPaiementId == mp.moyenPaiement) {
          value.moyenPaiementId = mp.id;
          this.search(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  search(value) {
    const nom = this.chaine.split(' ');
    if (nom.length) {
      this.adherentService.getAdherentByNom(nom[0]).subscribe((adherent) => {
        this.adherentsByNom = adherent;
        this.searchAdherent = this.adherentsByNom;
        for (let a of this.adherentsByNom) {
          this.adherentNomAndPrenom = a.nom + " " + a.prenom;
          if (value.adherentId == a.nom + " " + a.prenom) {
            this.currentAdherentId = value.adherentId;
            value.adherentId = a.id;
            this.save(value);
          }
        }
      });
    } else {
      this.searchAdherent = [];
    }
  }

  getNomAdherent(value) {
    this.adherentService.getAdherents().subscribe(data => {
      this.adherents = data;
      for (let a of this.adherents) {
        if (value.adherentId == a.numAdherent + a.nom + a.prenom) {
          value.adherentId = a.id;
        }
      }
    }, error => {
      console.log(error);
    });
  }

  save(value) {
    this.cotisationService.addCotisation(value).subscribe(data => {
      this.cotisations = data;
      this.adherentNomAndPrenom = '';
      this.router.navigate(['infosAdherent/' + this.cotisations.adherentId]);
    }, error => {
      console.log(error);
    });
  }

  onSaveCotisation(value) {
    this.getMoyenPaiements(value);
  }
}
