import { Component, OnInit } from '@angular/core';
import {EvenementService} from "../services/evenement.service";
import {AdherentService} from "../services/adherent.service";
import {Adherent} from "../model/adherent.model";
import {JwtClientService} from "../services/jwt-client.service";

@Component({
  selector: 'app-evenement',
  templateUrl: './evenement.component.html',
  styleUrls: ['./evenement.component.css']
})
export class EvenementComponent implements OnInit {

  evenements;
  list;
  adherent;
  utilisateur;
  adherentList;
  chaine = "";
  natureEvenements;
  adherentsByNom;
  searchAdherent: Adherent[];
  public size:number = 5;
  public currentPage:number = 0;
  public totalPages:number;
  public pages:Array<number>;
  public currentNatureEvenement: string = "";
  public currentAdherentId: number = 0;
  evenementsList;
  adherentNomAndPrenom;

  constructor(private evenementService: EvenementService,
              private adherentService: AdherentService,
              private jwtService: JwtClientService) { }

  ngOnInit(): void {
    this.utilisateur = this.jwtService.utilisateur;

    this.adherentService.getAdherents().subscribe(data => {
      this.adherentList = data;
    }, error => {
      console.log(error);
    });

    this.evenementService.getNatureEvenements().subscribe(data => {
      this.natureEvenements = data;
    }, error => {
      console.log(error);
    });

    this.evenementService.getEvenements().subscribe(data => {
      this.evenements = data;
      for (let e of this.evenements) {
        this.adherentService.getAdherentById(e.adherentId).subscribe(data => {
          this.adherent = data;
          e.nomAdherent = this.adherent.nom;
        });
      }
    }, error => {
      console.log(error);
    });

    this.searchEvenement();
  }

  onSearch(value: any) {
    this.currentPage = 0;
    this.currentNatureEvenement = value.natureEvenement;
    this.searchAdherent = [];
    this.search(value);
    this.chaine = "";
  }

  searchEvenement() {
    this.evenementService.getEvenementsByKeyword(this.currentNatureEvenement, this.currentAdherentId, this.currentPage, this.size)
      .subscribe(data => {
        this.totalPages = data["totalPages"];
        this.pages = new Array<number>(this.totalPages);
        this.evenementsList = data["content"];
        for (let e of this.evenementsList) {
          this.getAdherentById(e);
        }
      }, error => {
        console.log(error)
      });
  }

  getAdherentById(e) {
    this.adherentService.getAdherentById(e.adherentId).subscribe(data => {
      this.adherent = data;
      if (e.adherentId == this.adherent.id) {
        e.nomAdherent = this.adherent.nom + " " + this.adherent.prenom
      }
    }, error => {
      console.log(error);
    });
  }

  onPageEvenement(i: number) {
    this.currentPage = i;
    this.searchEvenement();
  }

  search(value) {
    const nom = this.chaine.split(' ');
    if (this.chaine != "") {
      this.adherentService.getAdherentByNom(nom[0]).subscribe((adherent) => {
        this.adherentsByNom = adherent;
        this.searchAdherent = this.adherentsByNom;
        for (let a of this.adherentsByNom) {
          this.adherentNomAndPrenom = a.nom + " " + a.prenom;
          if (value.adherentId == a.nom + " " + a.prenom) {
            this.currentAdherentId = a.id;
            this.chaine = "";
          }
        }
        this.searchEvenement();
      });
    } else {
      this.searchAdherent = [];
      if (value.adherentId == "") {
        this.currentAdherentId = 0;
      }
      this.searchEvenement();
    }
  }

  onDeleteEvenement(e) {
    let confirmation = confirm("Êtes-vous sûr de vouloir le supprimer ?");
    if (confirmation) {
      this.evenementService.deleteEvenement(e.id).subscribe(
        data => {
          this.searchEvenement();
        }, error => {
          console.log(error);
        });
    }
  }
}
