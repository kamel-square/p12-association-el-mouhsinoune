import { Component, OnInit } from '@angular/core';
import {AdherentService} from "../services/adherent.service";
import {JwtClientService} from "../services/jwt-client.service";

@Component({
  selector: 'app-adherents',
  templateUrl: './adherents.component.html',
  styleUrls: ['./adherents.component.css']
})
export class AdherentsComponent implements OnInit {

  adherents;
  utilisateur;
  public size:number = 5;
  public currentPage:number = 0;
  public totalPages:number;
  public pages:Array<number>;
  public currentNom: string = "";
  public currentPrenom: string = "";
  public currentNumAdherent: string = "";

  constructor(private adherentService:AdherentService,
              private jwtService: JwtClientService) { }

  ngOnInit(): void {
    this.searchAdherentByKeyword();
    this.utilisateur = this.jwtService.utilisateur;
  }

  onSearch(value: any) {
    this.currentPage = 0;
    this.currentNom = value.nom;
    this.currentPrenom = value.prenom;
    this.currentNumAdherent = value.numAdherent;
    this.searchAdherentByKeyword();
  }

  searchAdherentByKeyword() {
    this.adherentService.getAdherentsByKeyword(this.currentNumAdherent, this.currentNom, this.currentPrenom, this.currentPage, this.size)
      .subscribe(data => {
        this.totalPages = data["totalPages"];
        this.pages = new Array<number>(this.totalPages);
        this.adherents = data["content"];
      }, error => {
        console.log(error);
      });
  }

  onPageAdherent(i: number) {
    this.currentPage = i;
    this.searchAdherentByKeyword();
  }

  onDeleteAdherent(a) {
    let confirmation = confirm("Êtes-vous sûr de vouloir le supprimer ?");
    if (confirmation) {
      this.adherentService.deleteResource(a.id)
        .subscribe(data => {
          this.searchAdherentByKeyword();
        }, error => {
          console.log(error);
        });
    }
  }
}
