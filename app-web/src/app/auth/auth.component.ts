import { Component, OnInit } from '@angular/core';
import {JwtClientService} from "../services/jwt-client.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  utilisateur;
  response;
  authStatus: boolean;
  private authRequest: { password: string; pseudo: string };

  constructor(public jwtService: JwtClientService) { }

  ngOnInit(): void {
    this.authStatus = this.jwtService.isAuth;
  }

  onSignIn(value) {
    this.authRequest = {
      "pseudo": value.pseudo,
      "password": value.password
    };
    this.jwtService.signIn(this.authRequest, value.pseudo);
  }

}
