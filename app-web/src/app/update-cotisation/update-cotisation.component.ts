import { Component, OnInit } from '@angular/core';
import {CotisationService} from "../services/cotisation.service";
import {AdherentService} from "../services/adherent.service";
import {ActivatedRoute, Router} from "@angular/router";
import {EvenementService} from "../services/evenement.service";

@Component({
  selector: 'app-update-cotisation',
  templateUrl: './update-cotisation.component.html',
  styleUrls: ['./update-cotisation.component.css']
})
export class UpdateCotisationComponent implements OnInit {

  cotisation;
  adherent;
  currentNomAdherent;
  moyenPaiements;
  currentMoyenPaiement;
  moyen;

  constructor(private cotisationService: CotisationService,
              private adherentService: AdherentService,
              private evenementService: EvenementService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.cotisationService.getCotisationById(+id).subscribe(data => {
      this.cotisation = data;
      this.getAdherent(this.cotisation.adherentId);
      this.getMoyenPaiementById(this.cotisation.moyenPaiementId);
    }, error => {
      console.log(error);
    });

    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
    }, error => {
      console.log(error);
    });

  }

  getAdherent(id) {
    this.adherentService.getAdherentById(id).subscribe(data => {
      this.adherent = data;
      this.currentNomAdherent = this.adherent.nom;
    }, error => {
      console.log(error);
    });
  }

  getMoyenPaiementById(id) {
    this.evenementService.getMoyenPaiementById(id).subscribe(data => {
      this.moyen = data;
      this.currentMoyenPaiement = this.moyen.moyenPaiement;
    }, error => {
      console.log(error);
    });
  }

  onSave(value) {
    this.getMoyenPaiements(value);
    this.router.navigate(['cotisations'])
      .then(() => {
        window.location.reload();
      });
  }

  getMoyenPaiements(value) {
    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
      for (let mp of this.moyenPaiements) {
        if (mp.moyenPaiement == value.moyenPaiementId) {
          value.moyenPaiementId = mp.id;
          this.updateCotisation(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  updateCotisation(value) {
    this.cotisationService.updateCotisation(value).subscribe(data => {
      this.cotisation = data;
    });
  }

}
