import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCoutComponent } from './add-cout.component';

describe('AddCoutComponent', () => {
  let component: AddCoutComponent;
  let fixture: ComponentFixture<AddCoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddCoutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
