import { Component, OnInit } from '@angular/core';
import {EvenementService} from "../services/evenement.service";
import {AdherentService} from "../services/adherent.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-cout',
  templateUrl: './add-cout.component.html',
  styleUrls: ['./add-cout.component.css']
})
export class AddCoutComponent implements OnInit {

  adherents;
  evenements;
  natureCouts;
  natureEvenement;
  moyenPaiements;

  constructor(private evenementService: EvenementService,
              private adherentService: AdherentService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const evenementId = this.route.snapshot.params['evenementId'];

    this.evenementService.getEvenementById(evenementId).subscribe(data => {
      this.evenements = data;
      this.getAdherentById(this.evenements.adherentId);
    }, error => {
      console.log(error);
    });

    this.evenementService.getNatureCout().subscribe(data => {
      this.natureCouts = data;
    }, error => {
      console.log(error);
    });

    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
    }, error => {
      console.log(error);
    });
  }

  getAdherentById(id) {
    this.adherentService.getAdherentById(id).subscribe(data =>{
      this.adherents = data;
    }, error => {
      console.log(error);
    });
  }

  getEvenementByNatureEvenement(value) {
    this.evenementService.getEvenements().subscribe(data => {
      this.natureEvenement = data;
      for (let e of this.natureEvenement) {
        if (value.evenementId == e.natureEvenement.natureEvenement) {
          value.evenementId = e.id;
        }
      }
    }, error => {
      console.log(error);
    });
  }

  getMoyenPaiementId(value) {
    this.evenementService.getMoyenPaiements().subscribe(data => {
      this.moyenPaiements = data;
      for (let m of this.moyenPaiements) {
        if (m.moyenPaiement == value.moyenPaiementId) {
          value.moyenPaiementId = m.id;
          this.save(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  getNatureCoutId(value) {
    const evenementId = this.route.snapshot.params['evenementId'];
    value.evenementId = +evenementId;
    this.evenementService.getNatureCout().subscribe(data => {
      this.natureCouts = data;
      for (let nc of this.natureCouts) {
        if (nc.natureCout == value.natureCoutId) {
          value.natureCoutId = nc.id;
          console.log(value.natureCoutId);
          this.getMoyenPaiementId(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  save(value) {
    this.evenementService.addCout(value).subscribe(data => {
      console.log(value);
      this.router.navigate(['infosEvenement/' + this.evenements.id]);
    }, error => {
      console.log(error);
    });
  }

  onSaveCout(value) {
    this.getNatureCoutId(value);
  }
}
