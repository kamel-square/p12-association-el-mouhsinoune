import { Component, OnInit } from '@angular/core';
import {AdherentService} from "../services/adherent.service";
import {Adherent} from "../model/adherent.model";
import {Router} from "@angular/router";
import {JwtClientService} from "../services/jwt-client.service";
import {MonCompteComponent} from "../mon-compte/mon-compte.component";
import {FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {MustMatch} from "../match-validator/must-match.validator";

@Component({
  selector: 'app-add-adherent',
  templateUrl: './add-adherent.component.html',
  styleUrls: ['./add-adherent.component.css']
})
export class AddAdherentComponent implements OnInit {

  pays;
  adherentList;
  adherent: Adherent;
  userCreated;
  user;
  pseudo;
  password;
  authRequest: { password: any; pseudo: any };
  lastAdherentAdded;
  numAdherent;
  biggestNumAdherent;
  registerForm: FormGroup;
  submitted = false;
  errors = false;
  compareEmail;
  compare: boolean = false;

  constructor(public adherentService:AdherentService,
              private router: Router,
              public jwtService: JwtClientService,
              private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.adherentService.getPays().subscribe(data => {
      this.pays = data;
    }, error => {
      console.log(error);
    });

    this.adherentService.getAdherents().subscribe(data => {
      this.adherentList = data;
      let j = 0;
      for (let i = 0; i < this.adherentList.length; i++) {
        if (this.adherentList[i].numAdherent > this.adherentList[j].numAdherent) {
          this.biggestNumAdherent = [this.adherentList[i].numAdherent];
          j = i;
        }
      }
      this.lastAdherentAdded = Math.max(this.biggestNumAdherent) + 1;
    }, error => {
      console.log(error);
    });

    this.registerForm = this.formBuilder.group({
      statut: ['', Validators.required],
      pseudo: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', Validators.required],
      genreId: ['', Validators.required],
      nom: ['', Validators.required],
      prenom: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      adresse: ['', Validators.required],
      situation: ['', Validators.required],
      paysId: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue]
    }, {
      validator: [MustMatch('password', 'confirmPassword'),
        this.comparison('email')]
    });
  }

  comparison(email) {
    return (formGroup: FormGroup) => {
      this.adherentService.getAdherents().subscribe(data => {
        this.compareEmail = data;
        for (let c of this.compareEmail) {
        const control = formGroup.controls[email];
            if (c.email == control.value) {
        if (control.errors && !control.errors.comparison) {
          // return if another validator has already found an error on the matchingControl
          return;
        }

        // set error on matchingControl if validation fails
        if (control.value == c.email) {
          control.setErrors({ comparison: true });
        } else {
          control.setErrors(null);
        }
            this.compare = true;
          }
        }
      });
    }
  }

  get r() { return this.registerForm.controls; }

  public getNumAdherent(value) {
    value.numAdherent = this.lastAdherentAdded + "";
  }

  public getPaysAndGenreId(value) {
    this.adherentService.getPays().subscribe(data => {
      this.pays = data;
      this.getGenreId(value);
      this.getNumAdherent(value);
      this.saveResource(value, this.pays);
    }, error => {
      console.log(error);
    });
  }

  public getGenreId(value) {
    if (value.genreId == 'Homme') {
      value.genreId = 1;
    } else if (value.genreId == 'Femme'){
      value.genreId = 2;
    }
  }

  public generateToken(authRequest) {
    let resp = this.jwtService.generateToken(authRequest);
    resp.subscribe(data => {
      this.getUtilisateurByPseudo(data);
    });
  }

  public getUtilisateurByPseudo(token) {
    this.jwtService.getUtilisateurByPseudo(token, this.pseudo).subscribe(data => {
      this.user = data;
      this.user = JSON.parse(this.user);
    });
  }

  public saveResource(value, country) {

    this.authRequest = {
      "pseudo": this.pseudo,
      "password": this.password
    };

    this.generateToken(this.authRequest);
    for (let p of country) {
      if (value.paysId == p.pays) {
        value.paysId = p.id;
        value.utilisateurId = this.user.id;
        this.adherentService.saveAdherent(value).subscribe(data => {
          this.adherent = data;
          if (this.jwtService.utilisateur) {
            this.router.navigate(['/infosAdherent/' + this.adherent.id]);
          } else {
            this.router.navigate(['/monCompte/' + this.adherent.utilisateurId]);
          }
        }, error => {
          console.log(error);
        });
      }
    }
  }

  onSaveAdherent(value) {
    this.submitted = true;

    if (this.r.genreId.errors || this.r.nom.errors || this.r.prenom.errors || this.r.email.errors
      || this.r.adresse.errors || this.r.situation.errors || this.r.paysId.errors) {
      return;
    }
      this.getPaysAndGenreId(value);
  }

  onSaveUser(value) {
    this.pseudo = value.pseudo;
    this.password = value.password;
    value.actif = true;
    if (!this.jwtService.utilisateur) {
      value.statut = 'Adhérent';
    }
    if (value.pseudo == "") {
      value.pseudo = null
    }
    if (value.password == "") {
      value.password = null
    }
    if (value.statut == "") {
      value.statut = null
    }
    this.submitted = true;

    if (this.r.confirmPassword.errors || this.r.statut.errors || this.r.pseudo.errors || this.r.password.errors) {
      return;
    }
    this.jwtService.saveUser(value).subscribe(data => {
      this.user = data;
      this.userCreated = true;
      this.submitted = false;
    }, error => {
      console.log(error);
    });
  }
}
