import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMembreFamilleComponent } from './update-membre-famille.component';

describe('UpdateMembreFamilleComponent', () => {
  let component: UpdateMembreFamilleComponent;
  let fixture: ComponentFixture<UpdateMembreFamilleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateMembreFamilleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMembreFamilleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
