import { Component, OnInit } from '@angular/core';
import {JwtClientService} from "../services/jwt-client.service";
import {AdherentService} from "../services/adherent.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-update-membre-famille',
  templateUrl: './update-membre-famille.component.html',
  styleUrls: ['./update-membre-famille.component.css']
})
export class UpdateMembreFamilleComponent implements OnInit {

  membreFamille;
  adherent;
  currentLienParente;
  lienParentes;
  lien;

  constructor(public jwtService: JwtClientService,
              private adherentService: AdherentService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.adherentService.getMembreFamillesById(+id).subscribe(data => {
      this.membreFamille = data;
      this.getAdherent(this.membreFamille.adherentId);
      this.getLienParenteById(this.membreFamille.lienParenteId);
    }, error => {
      console.log(error);
    });

    this.adherentService.getLienParentes().subscribe(data => {
      this.lienParentes = data;
    }, error => {
      console.log(error);
    });
  }

  getLienParenteById(id) {
    this.adherentService.getLienParenteById(id).subscribe(data => {
      this.lien = data;
      this.currentLienParente = this.lien.lien;
    }, error => {
      console.log(error);
    });
  }

  getAdherent(id) {
    this.adherentService.getAdherentById(id).subscribe(data => {
      this.adherent = data;
    }, error => {
      console.log(error);
    });
  }

  onSave(value) {
    this.getLienParentes(value);
    if (this.jwtService.utilisateur.statut == 'Responsable') {
      this.router.navigate(['infosAdherent/' + this.membreFamille.adherentId]).then(() => {
        window.location.reload();
      });
    } else {
      this.router.navigate(['monCompte/' + this.membreFamille.adherentId]).then(() => {
        window.location.reload();
      });
    }
  }

  getLienParentes(value) {
    this.adherentService.getLienParentes().subscribe(data => {
      this.lienParentes = data;
      for (let lp of this.lienParentes) {
        if (lp.lien == value.lienParenteId) {
          value.lienParenteId = lp.id;
          this.updateMembreFamille(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  updateMembreFamille(value) {
    this.adherentService.updateMembreFamille(value).subscribe(data => {
      this.membreFamille = data;
    });
  }
}
