import {Component, OnInit} from '@angular/core';
import {JwtClientService} from "./services/jwt-client.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(public jwtService:JwtClientService,
              private router: Router) {}

  ngOnInit(): void {
    this.jwtService.loadAuthenticatedUserFromLocalStorage();
  }

  logout() {
    this.jwtService.removeTokenFromLocalStorage();
    if (!this.jwtService.isAuth) {
      alert('Vous êtes déconnecté.');
      this.router.navigate(['auth']);
    }
  }
}
