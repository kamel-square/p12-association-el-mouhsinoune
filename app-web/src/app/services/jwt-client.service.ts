import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class JwtClientService implements CanActivate {

  public host: string = "http://localhost:8888/authentification";
  isAuth: boolean;
  private value;
  public utilisateur;
  authenticatedUser: boolean;
  private saveToken;

  constructor(private http: HttpClient,
              private router: Router) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.isAuth == true) {
      return true;
    } else {
      this.router.navigate(['/auth']);
    }
  }

  signIn(authRequest, pseudo) {
    return new Promise(
      (resolve, reject) => {
        setTimeout(
          () => {
            let resp = this.generateToken(authRequest);
            resp.subscribe(data => {
              this.accessApi(data, pseudo);
            });
            this.loadAuthenticatedUserFromLocalStorage();
            resolve(true);
          }, 200
        );
      }
    );
  }

  signOut() {
    this.isAuth = false;
  }

  public generateToken(request) {
    return this.http.post(this.host + "/authenticate", request, {responseType: 'text' as 'json'});
  }

  public accessApi(token, pseudo) {
    this.isAuth = true;
    this.getUtilisateurByPseudo(token, pseudo).subscribe(data => {
      this.value = data;
      this.utilisateur = JSON.parse(this.value);
      this.authenticatedUser = true;
      this.saveToken = {id: this.utilisateur.id, pseudo: pseudo, statut: this.utilisateur.statut};
      this.saveAuthenticatedUser();
    });
  }

  public isResponsable() {
    if (this.utilisateur) {
      if (this.utilisateur.statut == 'Responsable')
        return true;
    }
    return false;
  }

  public saveAuthenticatedUser() {
    localStorage.setItem("authToken", btoa(JSON.stringify(this.saveToken)));
    if (this.isAuth) {
      this.router.navigate(['/']);
    }
  }

  public loadAuthenticatedUserFromLocalStorage() {
    let token = localStorage.getItem('authToken');
    if (token) {
      let user = JSON.parse(atob(token));
      this.utilisateur = {id: user.id, pseudo: user.pseudo, statut: user.statut};
      this.authenticatedUser = true;
      this.isAuth = true;
      this.saveToken = token;
    }
  }

  public removeTokenFromLocalStorage() {
    localStorage.removeItem('authToken');
    this.isAuth = false;
    this.saveToken = undefined;
    this.utilisateur = undefined;
  }

  public getUtilisateurs(token) {
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set("Authorization", tokenStr);
    return this.http.get(this.host + "/listeUtilisateurs", {headers, responseType: 'text' as 'json'});
  }

  public getUtilisateursById(token, id) {
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set("Authorization", tokenStr);
    return this.http.get(this.host + "/utilisateur/" + id, {headers, responseType: 'text' as 'json'});
  }

  public getUtilisateurByPseudo(token, pseudo) {
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set("Authorization", tokenStr);
    return this.http.get(this.host + "/utilisateurByPseudo/" + pseudo, {headers, responseType: 'text' as 'json'});
  }

  public saveUser(user) {
    return this.http.post(this.host + "/saveUtilisateur", user);
  }

  public deleteUser(token, id) {
    let tokenStr = 'Bearer ' + token;
    const headers = new HttpHeaders().set("Authorization", tokenStr);
    return this.http.delete(this.host + "/deleteUtilisateur/" + id, {headers, responseType: 'text' as 'json'});
  }

}
