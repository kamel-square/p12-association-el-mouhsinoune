import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Cotisation} from "../model/cotisation.model";

@Injectable({
  providedIn: 'root'
})
export class CotisationService {

  public host: string = "http://localhost:8888/cotisation";

  constructor(private http: HttpClient) {
  }

  public getCotisations() {
    return this.http.get(this.host + "/cotisations");
  }

  public getCotisationsByKeyword(date: string, adherentId: number, moyenPaiementId: number, page: number, size: number): Observable<Cotisation> {
    return this.http.get<Cotisation>(this.host + "/searchCotisations?date=" + date + "&adherentId=" + adherentId +
      "&moyenPaiementId=" + moyenPaiementId + "&page=" + page + "&size=" + size);
  }

  public getCotisationById(id) {
    return this.http.get(this.host + "/cotisations/" + id);
  }

  public getCotisationsByAdherentId(adherentId) {
    return this.http.get(this.host + "/cotisationsByAdherentId/" + adherentId);
  }

  public updateCotisation(value) {
    return this.http.put(this.host + "/updateCotisation", value);
  }

  public addCotisation(value) {
    return this.http.post(this.host + "/addCotisation", value);
  }

  public deleteCotisation(id) {
    return this.http.delete(this.host + "/deleteCotisation/" + id)
  }
}
