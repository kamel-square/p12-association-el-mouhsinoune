import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Adherent} from "../model/adherent.model";

@Injectable({
  providedIn: 'root'
})
export class AdherentService {

  public host: string = "http://localhost:8888/adherent";

  constructor(private httpClient: HttpClient) {
  }

  public getAdherents() {
    return this.httpClient.get(this.host + "/adherents");
  }

  public getAdherentsByKeyword(numAdherent: string, nom: string, prenom: string, page: number, size: number) {
    return this.httpClient.get(this.host + "/searchAdherent?numAdherent=" + numAdherent + "&nom=" + nom +
      "&prenom=" + prenom + "&page=" + page + "&size=" + size);
  }

  public getAdherentById(id): Observable<Adherent> {
    return this.httpClient.get<Adherent>(this.host + "/adherent/" + id);
  }

  public getAdherentByNom(nom): Observable<Adherent> {
    return this.httpClient.get<Adherent>(this.host + "/getAdherentByNom?nom=" + nom)
  }

  public getAdherentByUtilisateurId(utilisateurId) {
    return this.httpClient.get(this.host + "/getAdherentByUtilisateurId/" + utilisateurId);
  }

  public deleteResource(id) {
    return this.httpClient.delete(this.host + "/deleteAdherent/" + id);
  }

  public saveAdherent(value): Observable<Adherent> {
    return this.httpClient.post<Adherent>(this.host + "/saveAdherent", value);
  }

  public updateAdherent(value): Observable<Adherent> {
    return this.httpClient.put<Adherent>(this.host + "/updateAdherent", value)
  }

  public getPays() {
    return this.httpClient.get(this.host + "/pays");
  }

  public getPaysById(id) {
    return this.httpClient.get(this.host + "/pays/" + id);
  }

  public getMembreFamilles() {
    return this.httpClient.get(this.host + "/membreFamilles");
  }

  public getMembreFamillesByAdherentId(adherentId) {
    return this.httpClient.get(this.host + "/getMembreFamillesByAdherentId/" + adherentId);
  }

  public getLienParentes() {
    return this.httpClient.get(this.host + "/lienParentes");
  }

  public saveMembreFamille(membreFamille) {
    return this.httpClient.post(this.host + "/saveMembre", membreFamille);
  }

  public updateMembreFamille(membreFamille) {
    return this.httpClient.put(this.host + "/updateMembre", membreFamille);
  }

  public deleteMembreFamille(id) {
    return this.httpClient.delete(this.host + "/deleteMembreFamille/" + id);
  }

  public getMembreFamillesById(id) {
    return this.httpClient.get(this.host + "/membreFamilles/" + id);
  }

  getLienParenteById(id) {
    return this.httpClient.get(this.host + "/lienParentes/" + id);
  }
}
