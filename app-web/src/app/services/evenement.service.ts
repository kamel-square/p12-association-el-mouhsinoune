import { Injectable } from '@angular/core';
import {HttpClient, HttpEvent, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EvenementService {

  public host:string = "http://localhost:8888/evenement";

  constructor(private http: HttpClient) { }

  public getEvenements() {
    return this.http.get(this.host + "/evenements");
  }

  public getEvenementsByKeyword(natureEvenement:string, adherentId:number, page:number, size:number) {
    return this.http.get(this.host + "/searchEvenement?natureEvenement=" + natureEvenement +
                          "&adherentId=" + adherentId + "&page=" + page + "&size=" + size);
  }

  public getEvenementById(id) {
    return this.http.get(this.host + "/evenements/" + id);
  }

  public getEvenementsByAdherentId(adherentId) {
    return this.http.get(this.host + "/getEvenementsByAdherentId/" + adherentId);
  }

  public getNatureEvenements() {
    return this.http.get(this.host + "/natureEvenements");
  }

  public getNatureEvenementById(id) {
    return this.http.get(this.host + "/natureEvenements/" + id);
  }

  public addEvenement(value) {
    return this.http.post(this.host + "/addEvenement", value);
  }

  public updateEvenement(value) {
    return this.http.put(this.host + "/updateEvenement", value);
  }

  public deleteEvenement(id) {
    return this.http.delete(this.host + "/deleteEvenement/" + id);
  }

  public getMoyenPaiements() {
    return this.http.get(this.host + "/moyenPaiements");
  }

  public getMoyenPaiementById(id) {
    return this.http.get(this.host + "/moyenPaiements/" + id)
  }

  public getCouts() {
    return this.http.get(this.host + "/couts");
  }

  public getCoutById(id) {
    return this.http.get(this.host + "/couts/" + id);
  }

  public getCoutsByEvenementId(evenementId) {
    return this.http.get(this.host + "/getCoutsByEvenementId/" + evenementId)
  }

  public getNatureCout() {
    return this.http.get(this.host + "/natureCouts");
  }

  public getNatureCoutById(id) {
    return this.http.get(this.host + "/natureCouts/" + id);
  }

  public addCout(value) {
    return this.http.post(this.host + "/saveCout", value);
  }

  public getNatureDocuments() {
    return this.http.get(this.host + "/natureDocuments");
  }

  public getNatureDocumentById(id) {
    return this.http.get(this.host + "/natureDocuments/" + id);
  }

  public getDocuments() {
    return this.http.get(this.host + "/documents");
  }

  public getDocumentById(id) {
    return this.http.get(this.host + "/documents/" + id);
  }

  public getFile(id) {
    return this.host + "/getDocument/" + id;
  }

  public getDocumentsByEvenementId(evenementId) {
    return this.http.get(this.host + "/getDocumentsByEvenementId/" + evenementId);
  }

  public uploadFile(file: File, id): Observable<HttpEvent<{}>> {
    let formData: FormData = new FormData();
    formData.append('file', file);

    const req = new HttpRequest('POST', this.host + "/uploadFile/" + id, formData, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }

  public addDocument(value) {
    return this.http.post(this.host + "/addDocument", value);
  }

  public updateCout(value) {
    return this.http.put(this.host + "/updateCout", value);
  }

  public updateDocument(value) {
    return this.http.put(this.host + "/updateDocument", value);
  }

  public deleteCout(id) {
    return this.http.delete(this.host + "/deleteCout/" + id);
  }

  public deleteDocument(id) {
    return this.http.delete(this.host + "/deleteDocument/" + id);
  }
}
