import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InfosEvenementComponent } from './infos-evenement.component';

describe('InfosEvenementComponent', () => {
  let component: InfosEvenementComponent;
  let fixture: ComponentFixture<InfosEvenementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InfosEvenementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InfosEvenementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
