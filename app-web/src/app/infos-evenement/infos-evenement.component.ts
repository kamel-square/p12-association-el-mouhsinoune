import { Component, OnInit } from '@angular/core';
import {EvenementService} from "../services/evenement.service";
import {ActivatedRoute} from "@angular/router";
import {AdherentService} from "../services/adherent.service";
import {CotisationService} from "../services/cotisation.service";
import {JwtClientService} from "../services/jwt-client.service";

@Component({
  selector: 'app-infos-evenement',
  templateUrl: './infos-evenement.component.html',
  styleUrls: ['./infos-evenement.component.css']
})
export class InfosEvenementComponent implements OnInit {

  couts;
  adherent;
  utilisateur;
  evenement;
  evenementCout;
  documents;
  file;
  private pdfSource;
  open: boolean = false;

  constructor(private evenementService: EvenementService,
              private adherentService: AdherentService,
              private route: ActivatedRoute,
              private jwtService: JwtClientService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.utilisateur = this.jwtService.utilisateur;
    this.getEvenement(+id);
    this.getCouts(+id);
    this.getDocuments(+id);
  }

  getEvenement(id) {
    this.evenementService.getEvenementById(id).subscribe(data => {
      this.evenement = data;
      this.getAdherent(this.evenement.adherentId);
    }, error => {
      console.log(error);
    });
  }

  getAdherent(id) {
    this.adherentService.getAdherentById(id).subscribe(data => {
      this.adherent = data;
    }, error => {
      console.log(error);
    });
  }

  getCouts(evenementId) {
    this.evenementService.getCoutsByEvenementId(evenementId).subscribe(data => {
      this.couts = data;
    }, error => {
      console.log(error);
    });
  }

  getFile(id) {
    this.pdfSource = this.evenementService.getFile(id);
    window.open(this.pdfSource);
    this.open = true;
  }

  getDocuments(evenementId) {
    this.evenementService.getDocumentsByEvenementId(evenementId).subscribe(data => {
      this.documents = data;
    }, error => {
      console.log(error);
    });
  }

  onDeleteDocument(d) {
    let confirmation = confirm("Êtes-vous sûr de vouloir le supprimer ?");
    if (confirmation) {
      this.evenementService.deleteDocument(d.id)
        .subscribe(data => {
          location.reload();
        }, error => {
          console.log(error);
        });
    }
  }

  onDeleteCout(c) {
    let confirmation = confirm("Êtes-vous sûr de vouloir le supprimer ?");
    if (confirmation) {
      this.evenementService.deleteCout(c.id)
        .subscribe(data => {
          location.reload();
        }, error => {
          console.log(error);
        });
    }
  }
}
