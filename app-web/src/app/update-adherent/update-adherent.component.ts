import { Component, OnInit } from '@angular/core';
import {AdherentService} from "../services/adherent.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Adherent} from "../model/adherent.model";
import {AdherentsComponent} from "../adherents/adherents.component";
import {JwtClientService} from "../services/jwt-client.service";

@Component({
  selector: 'app-update-adherent',
  templateUrl: './update-adherent.component.html',
  styleUrls: ['./update-adherent.component.css']
})
export class UpdateAdherentComponent implements OnInit {

  adherent: Adherent;
  pays;
  paysById;
  nomPays;
  genre: string;

  constructor(private adherentService: AdherentService,
              private route: ActivatedRoute,
              private router: Router,
              public jwtService: JwtClientService) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.adherentService.getAdherentById(+id).subscribe(data => {
      this.adherent = data;
      this.getGenre(this.adherent);
      this.getPaysById(this.adherent.paysId);
    }, error => {
      console.log(error);
    });

    this.adherentService.getPays().subscribe(data => {
      this.pays = data;
    }, error => {
      console.log(error);
    });
  }

  public getGenre(adherent) {
    if (adherent.genreId == 1) {
      this.genre = 'Homme';
    } else {
      this.genre = 'Femme';
    }
  }

  public getPaysById(id) {
    this.adherentService.getPaysById(id).subscribe(data => {
      this.paysById = data;
      this.nomPays = this.paysById.pays;
    }, error => {
      console.log(error);
    });
  }

  public getPaysIdOfAdherent(value) {
    this.adherentService.getPays().subscribe(data => {
      this.pays = data;
      for (let p of this.pays) {
        if (p.pays == value.paysId) {
          value.paysId = p.id;
          this.updateAdherent(value);
        }
      }
    }, error => {
      console.log(error);
    });
  }

  onSave(value) {
    this.getPaysIdOfAdherent(value);
    this.router.navigate(['adherents'])
      .then(() => {
        window.location.reload();
    });
  }

  updateAdherent(value) {
    this.adherentService.updateAdherent(value).subscribe(data => {
    }, error => {
      console.log(error);
    });
  }
}
