package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.Cout;
import fr.association.elmouhsinoune.evenement.entities.Evenement;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(EvenementController.class)
@AutoConfigureMockMvc
public class EvenementControllerTest {

    @MockBean
    private EvenementController controller;

    @Autowired
    private MockMvc mockMvc;

    private static Evenement evenement;

    @BeforeAll
    public static void init() {
        evenement = new Evenement();
        evenement.setNatureEvenementId(Long.valueOf(1));
        evenement.setCommentaire("Commentaire");
        evenement.setAdherentId(Long.valueOf(1));
    }

    @Test
    void getEvenementById() throws Exception {
        // Given
        when(controller.getEvenementById(anyLong())).thenReturn(evenement);

        // When
        RequestBuilder builder = MockMvcRequestBuilders.get("/evenements/50");

        // Then
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.jsonPath("$.adherentId").value(Long.valueOf(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.commentaire").value("Commentaire"))
                .andExpect(status().isOk());

        verify(controller).getEvenementById(anyLong());

    }
}
