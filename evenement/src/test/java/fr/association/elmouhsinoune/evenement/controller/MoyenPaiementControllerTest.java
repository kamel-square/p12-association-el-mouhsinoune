package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.Evenement;
import fr.association.elmouhsinoune.evenement.entities.MoyenPaiement;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MoyenPaiementController.class)
@AutoConfigureMockMvc
public class MoyenPaiementControllerTest {

    @MockBean
    private MoyenPaiementController controller;

    @Autowired
    private MockMvc mockMvc;

    private static MoyenPaiement moyenPaiement;

    @BeforeAll
    public static void init() {
        moyenPaiement = new MoyenPaiement();
        moyenPaiement.setMoyenPaiement("Espèce");
    }

    @Test
    void getMoyenPaiementById() throws Exception {
        // Given
        when(controller.getMoyenPaiementById(anyLong())).thenReturn(moyenPaiement);

        // When
        RequestBuilder builder = MockMvcRequestBuilders.get("/moyenPaiements/50");

        // Then
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.jsonPath("$.moyenPaiement").value("Espèce"))
                .andExpect(status().isOk());

        verify(controller).getMoyenPaiementById(anyLong());

    }
}
