package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.Cout;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CoutController.class)
@AutoConfigureMockMvc
public class CoutControllerTest {

    @MockBean
    private CoutController controller;

    @Autowired
    private MockMvc mockMvc;

    private static Cout cout;

    @BeforeAll
    public static void init() {
        cout = new Cout();
        cout.setNatureCoutId(1);
        cout.setCommentaire("Commentaire");
        cout.setDate(new Date());
        cout.setEvenementId(1);
        cout.setMoyenPaiementId(1);
    }

    @Test
    void getCoutById() throws Exception {
        // Given
        when(controller.getCoutById(anyLong())).thenReturn(cout);

        // When
        RequestBuilder builder = MockMvcRequestBuilders.get("/couts/50");

        // Then
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.jsonPath("$.evenementId").value(Long.valueOf(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.commentaire").value("Commentaire"))
                .andExpect(status().isOk());

        verify(controller).getCoutById(anyLong());

    }
}
