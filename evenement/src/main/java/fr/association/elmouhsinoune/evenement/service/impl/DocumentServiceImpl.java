package fr.association.elmouhsinoune.evenement.service.impl;

import fr.association.elmouhsinoune.evenement.dao.DocumentRepository;
import fr.association.elmouhsinoune.evenement.entities.Document;
import fr.association.elmouhsinoune.evenement.service.contract.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentRepository repository;

    @Override
    public List<Document> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Document> findByEvenementId(long evenementId) {
        return repository.findByEvenementId(evenementId);
    }

    @Override
    public Document findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public Document save(Document document) {
        return repository.save(document);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }
}
