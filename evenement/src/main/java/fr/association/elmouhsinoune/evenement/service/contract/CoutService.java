package fr.association.elmouhsinoune.evenement.service.contract;

import fr.association.elmouhsinoune.evenement.entities.Cout;

import java.util.List;

public interface CoutService {

    List<Cout> findAll();
    List<Cout> findByEvenementId(long evenementId);
    Cout findById(long id);
    Cout save(Cout cout);
    void delete(long id);
}
