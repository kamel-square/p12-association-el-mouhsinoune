package fr.association.elmouhsinoune.evenement.service.contract;

import fr.association.elmouhsinoune.evenement.entities.Evenement;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EvenementService {

    List<Evenement> findAll();
    List<Evenement> findByAdherentId(long adherentId);
    Page<Evenement> search(String natureEvenement, Long adherentId, int page, int size);
    Evenement findById(long id);
    Evenement save(Evenement evenement);
    void delete(long id);
}
