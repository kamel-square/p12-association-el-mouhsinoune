package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.NatureCout;
import fr.association.elmouhsinoune.evenement.service.contract.NatureCoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class NatureCoutController {

    @Autowired
    private NatureCoutService service;

    /**
     * Affiche la liste des natures des coûts.
     */
    @GetMapping(value = "/natureCouts")
    public List<NatureCout> getNatureCouts() {
        return service.findAll();
    }

    /**
     * Affiche la nature du coût de par son id.
     */
    @GetMapping(value = "/natureCouts/{id}")
    public NatureCout getNatureCoutById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Ajoute une nature d'un coût.
     */
    @PostMapping("/addNatureCout")
    public NatureCout addNatureCout(@RequestBody NatureCout natureCout) {
        return service.save(natureCout);
    }

    /**
     * Supprime une nature d'un coût.
     */
    @DeleteMapping(value = "/deleteAdherent/{id}")
    public void deleteNatureCout(@PathVariable("id") long id) {
        service.delete(id);
    }
}
