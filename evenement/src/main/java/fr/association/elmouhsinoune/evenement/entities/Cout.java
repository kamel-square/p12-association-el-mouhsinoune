package fr.association.elmouhsinoune.evenement.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Cout implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private double cout;
    private String commentaire;
    private Date date;
    @Column(name = "nature_cout_id")
    private long natureCoutId;
    @Column(name = "moyen_paiement_id")
    private long moyenPaiementId;
    @Column(name = "evenement_id")
    private long evenementId;

    @ManyToOne
    @JoinColumn(name = "nature_cout_id", referencedColumnName = "id", insertable= false, updatable= false)
    private NatureCout natureCout;

    @ManyToOne
    @JoinColumn(name = "moyen_paiement_id", referencedColumnName = "id", insertable= false, updatable= false)
    private MoyenPaiement moyenPaiement;

    @ManyToOne
    @JoinColumn(name = "evenement_id", referencedColumnName = "id", insertable= false, updatable= false)
    private Evenement evenement;
}
