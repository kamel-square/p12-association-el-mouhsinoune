package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.NatureDocument;
import fr.association.elmouhsinoune.evenement.service.contract.NatureDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NatureDocumentController {

    @Autowired
    private NatureDocumentService service;

    /**
     * Affiche la liste des natures des documents.
     */
    @GetMapping(value = "/natureDocuments")
    public List<NatureDocument> getNatureDocuments() {
        return service.findAll();
    }

    /**
     * Affiche la nature de document de par son id.
     */
    @GetMapping(value = "/natureDocuments/{id}")
    public NatureDocument getNatureDocumentById(@PathVariable("id") long id) {
        return service.findById(id);
    }
}
