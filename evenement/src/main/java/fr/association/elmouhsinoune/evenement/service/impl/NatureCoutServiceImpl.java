package fr.association.elmouhsinoune.evenement.service.impl;

import fr.association.elmouhsinoune.evenement.dao.NatureCoutRepository;
import fr.association.elmouhsinoune.evenement.entities.NatureCout;
import fr.association.elmouhsinoune.evenement.service.contract.NatureCoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NatureCoutServiceImpl implements NatureCoutService {

    @Autowired
    private NatureCoutRepository repository;

    @Override
    public List<NatureCout> findAll() {
        return repository.findAll();
    }

    @Override
    public NatureCout findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public NatureCout save(NatureCout natureCout) {
        return repository.save(natureCout);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }
}
