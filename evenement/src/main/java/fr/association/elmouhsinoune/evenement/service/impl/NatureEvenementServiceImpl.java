package fr.association.elmouhsinoune.evenement.service.impl;

import fr.association.elmouhsinoune.evenement.dao.NatureEvenementRepository;
import fr.association.elmouhsinoune.evenement.entities.NatureEvenement;
import fr.association.elmouhsinoune.evenement.service.contract.NatureEvenementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NatureEvenementServiceImpl implements NatureEvenementService {

    @Autowired
    private NatureEvenementRepository repository;

    @Override
    public List<NatureEvenement> findAll() {
        return repository.findAll();
    }

    @Override
    public NatureEvenement findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public NatureEvenement save(NatureEvenement natureEvenement) {
        return repository.save(natureEvenement);
    }
}
