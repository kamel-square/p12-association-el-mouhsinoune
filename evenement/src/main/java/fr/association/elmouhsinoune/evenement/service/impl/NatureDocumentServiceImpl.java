package fr.association.elmouhsinoune.evenement.service.impl;

import fr.association.elmouhsinoune.evenement.dao.NatureDocumentRepository;
import fr.association.elmouhsinoune.evenement.entities.NatureDocument;
import fr.association.elmouhsinoune.evenement.service.contract.NatureDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NatureDocumentServiceImpl implements NatureDocumentService {

    @Autowired
    private NatureDocumentRepository repository;

    @Override
    public List<NatureDocument> findAll() {
        return repository.findAll();
    }

    @Override
    public NatureDocument findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public NatureDocument save(NatureDocument natureDocument) {
        return repository.save(natureDocument);
    }
}
