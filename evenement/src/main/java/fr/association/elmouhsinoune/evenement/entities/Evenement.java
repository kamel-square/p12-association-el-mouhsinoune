package fr.association.elmouhsinoune.evenement.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Evenement implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String commentaire;
    @Column(name = "nature_evenement_id")
    private Long natureEvenementId;
    @Column(name = "adherent_id")
    private Long adherentId;

    @Transient
    private String nomAdherent;
    @Transient
    private String prenomAdherent;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "evenement")
    private Set<Document> documents;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToMany(mappedBy = "evenement")
    private Set<Cout> couts;

    @ManyToOne
    @JoinColumn(name = "nature_evenement_id", referencedColumnName = "id", insertable= false, updatable= false)
    private NatureEvenement natureEvenement;

}
