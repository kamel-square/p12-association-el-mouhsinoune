package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.Evenement;
import fr.association.elmouhsinoune.evenement.entities.NatureEvenement;
import fr.association.elmouhsinoune.evenement.service.contract.EvenementService;
import fr.association.elmouhsinoune.evenement.service.contract.NatureEvenementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EvenementController {

    @Autowired
    private EvenementService service;

    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Affiche la liste des événements.
     */
    @GetMapping(value = "/evenements")
    public List<Evenement> evenementList() {
        return service.findAll();
    }

    /**
     * Affiche un événement de par son id.
     */
    @GetMapping(value = "/evenements/{id}")
    public Evenement getEvenementById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Affiche le résultat de la recherche.
     * @param natureEvenement
     * @param adherentId
     * @param page
     * @param size
     */
    @GetMapping(value = "/searchEvenement")
    public Page<Evenement> searchEvenement(@RequestParam(name = "natureEvenement", defaultValue = "") String natureEvenement,
                                           @RequestParam(name = "adherentId", defaultValue = "0") long adherentId,
                                           @RequestParam(name = "page", defaultValue = "0") int page,
                                           @RequestParam(name = "size", defaultValue = "10") int size) {

        if (!natureEvenement.equals("")) {
            log.info("La nature de l'événement est " + natureEvenement);
        }

        return service.search("%" + natureEvenement + "%", adherentId, page, size);
    }

    /**
     * Affiche la liste des événements de par l'adherentId.
     */
    @GetMapping(value = "/getEvenementsByAdherentId/{adherentId}")
    public List<Evenement> getEvenementsByAdherentId(@PathVariable("adherentId") long adherentId) {
        return service.findByAdherentId(adherentId);
    }

    /**
     * Ajoute un événement.
     */
    @PostMapping(value = "/addEvenement")
    public Evenement addEvenement(@RequestBody Evenement evenement) {
        return service.save(evenement);
    }

    /**
     * Modifie un événement.
     */
    @PutMapping(value = "/updateEvenement")
    public Evenement updateEvenement(@RequestBody Evenement evenement) {
        return service.save(evenement);
    }

    /**
     * Supprime un événement.
     */
    @DeleteMapping(value = "/deleteEvenement/{id}")
    public void deleteEvenement(@PathVariable("id") long id) {
        service.delete(id);
    }
}

