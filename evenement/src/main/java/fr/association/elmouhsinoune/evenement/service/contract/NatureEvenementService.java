package fr.association.elmouhsinoune.evenement.service.contract;

import fr.association.elmouhsinoune.evenement.entities.NatureEvenement;

import java.util.List;

public interface NatureEvenementService {

    List<NatureEvenement> findAll();
    NatureEvenement findById(long id);
    NatureEvenement save(NatureEvenement natureEvenement);
}
