package fr.association.elmouhsinoune.evenement.dao;

import fr.association.elmouhsinoune.evenement.entities.NatureDocument;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface NatureDocumentRepository extends JpaRepository<NatureDocument, Long> {
}
