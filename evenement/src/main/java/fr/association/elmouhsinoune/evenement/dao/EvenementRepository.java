package fr.association.elmouhsinoune.evenement.dao;

import fr.association.elmouhsinoune.evenement.entities.Evenement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface EvenementRepository extends JpaRepository<Evenement, Long> {

    List<Evenement> findByAdherentId(long adherentId);

    @Query("select e from Evenement e where lower(e.natureEvenement.natureEvenement) like lower(:natureEvenement) and e.adherentId =:adherentId")
    Page<Evenement> search(@Param("natureEvenement") String natureEvenement, @Param("adherentId") Long adherentId, Pageable pageable);

    @Query("select e from Evenement e where lower(e.natureEvenement.natureEvenement) like lower(:natureEvenement)")
    Page<Evenement> searchWithoutAdherentId(@Param("natureEvenement") String natureEvenement, Pageable pageable);
}
