package fr.association.elmouhsinoune.evenement.service.contract;

import fr.association.elmouhsinoune.evenement.entities.Document;

import java.util.List;

public interface DocumentService {

    List<Document> findAll();
    List<Document> findByEvenementId(long evenementId);
    Document findById(long id);
    Document save(Document document);
    void delete(long id);
}
