package fr.association.elmouhsinoune.evenement.service.impl;

import fr.association.elmouhsinoune.evenement.dao.MoyenPaiementRepository;
import fr.association.elmouhsinoune.evenement.entities.MoyenPaiement;
import fr.association.elmouhsinoune.evenement.service.contract.MoyenPaiementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoyenPaiementServiceImpl implements MoyenPaiementService {

    @Autowired
    private MoyenPaiementRepository repository;

    @Override
    public List<MoyenPaiement> findAll() {
        return repository.findAll();
    }

    @Override
    public MoyenPaiement findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public MoyenPaiement save(MoyenPaiement moyenPaiement) {
        return repository.save(moyenPaiement);
    }
}
