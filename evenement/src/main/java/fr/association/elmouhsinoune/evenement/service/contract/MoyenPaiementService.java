package fr.association.elmouhsinoune.evenement.service.contract;

import fr.association.elmouhsinoune.evenement.entities.MoyenPaiement;

import java.util.List;

public interface MoyenPaiementService {

    List<MoyenPaiement> findAll();
    MoyenPaiement findById(long id);
    MoyenPaiement save(MoyenPaiement moyenPaiement);
}
