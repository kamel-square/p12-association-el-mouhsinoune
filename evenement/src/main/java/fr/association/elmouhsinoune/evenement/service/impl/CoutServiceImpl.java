package fr.association.elmouhsinoune.evenement.service.impl;

import fr.association.elmouhsinoune.evenement.dao.CoutRepository;
import fr.association.elmouhsinoune.evenement.entities.Cout;
import fr.association.elmouhsinoune.evenement.service.contract.CoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoutServiceImpl implements CoutService {

    @Autowired
    private CoutRepository repository;

    @Override
    public List<Cout> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Cout> findByEvenementId(long evenementId) {
        return repository.findByEvenementId(evenementId);
    }

    @Override
    public Cout findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public Cout save(Cout cout) {
        return repository.save(cout);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }
}
