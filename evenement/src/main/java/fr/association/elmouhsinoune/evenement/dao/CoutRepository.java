package fr.association.elmouhsinoune.evenement.dao;

import fr.association.elmouhsinoune.evenement.entities.Cout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface CoutRepository extends JpaRepository<Cout, Long> {

    List<Cout> findByEvenementId(long evenementId);

}
