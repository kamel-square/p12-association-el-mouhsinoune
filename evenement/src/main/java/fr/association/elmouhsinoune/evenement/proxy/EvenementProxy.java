package fr.association.elmouhsinoune.evenement.proxy;

import fr.association.elmouhsinoune.evenement.beans.Adherent;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "gateway-server")
@RibbonClient(name = "evenement")
public interface EvenementProxy {

    /**
     * Récupère la liste des adhérents auprès du microservice adherent.
     */
    @GetMapping(value = "/adherent/adherents")
    List<Adherent> adherentList();

    /**
     * Récupère l'adhérent de par son id auprès du microservice adherent.
     */
    @GetMapping(value = "/adherent/adherent/{id}")
    Adherent getAdherentById(@PathVariable("id") long id);
}
