package fr.association.elmouhsinoune.evenement.service.contract;

import fr.association.elmouhsinoune.evenement.entities.NatureDocument;

import java.util.List;

public interface NatureDocumentService {

    List<NatureDocument> findAll();
    NatureDocument findById(long id);
    NatureDocument save(NatureDocument natureDocument);
}
