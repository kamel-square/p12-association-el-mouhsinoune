package fr.association.elmouhsinoune.evenement.dao;

import fr.association.elmouhsinoune.evenement.entities.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface DocumentRepository extends JpaRepository<Document, Long> {

    List<Document> findByEvenementId(long evenementId);

}
