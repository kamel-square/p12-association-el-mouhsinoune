package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.MoyenPaiement;
import fr.association.elmouhsinoune.evenement.service.contract.MoyenPaiementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MoyenPaiementController {

    @Autowired
    private MoyenPaiementService service;

    /**
     * Affiche la liste des moyens de paiement.
     */
    @GetMapping(value = "/moyenPaiements")
    public List<MoyenPaiement> moyenPaiementList() {
        return service.findAll();
    }

    /**
     * Affiche un moyen de paiement de par son id.
     */
    @GetMapping(value = "/moyenPaiements/{id}")
    public MoyenPaiement getMoyenPaiementById(@PathVariable("id") long id) {
        return service.findById(id);
    }
}
