package fr.association.elmouhsinoune.evenement.dao;

import fr.association.elmouhsinoune.evenement.entities.NatureEvenement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface NatureEvenementRepository extends JpaRepository<NatureEvenement, Long> {
    List<NatureEvenement> findAll();
}
