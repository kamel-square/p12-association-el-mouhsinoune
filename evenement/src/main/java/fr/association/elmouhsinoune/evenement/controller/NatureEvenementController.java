package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.NatureEvenement;
import fr.association.elmouhsinoune.evenement.service.contract.NatureEvenementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class NatureEvenementController {

    @Autowired
    private NatureEvenementService service;

    /**
     * Affiche la liste des natures d'événement.
     */
    @GetMapping(value = "/natureEvenements")
    public List<NatureEvenement> getNatureEvenements() {
        return service.findAll();
    }

    /**
     * Affiche la nature d'événement de par son id.
     */
    @GetMapping(value = "/natureEvenements/{id}")
    public NatureEvenement getNatureEvenementById(@PathVariable("id") long id) {
        return service.findById(id);
    }
}
