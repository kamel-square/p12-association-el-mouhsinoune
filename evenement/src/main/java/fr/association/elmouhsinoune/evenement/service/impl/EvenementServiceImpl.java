package fr.association.elmouhsinoune.evenement.service.impl;

import fr.association.elmouhsinoune.evenement.beans.Adherent;
import fr.association.elmouhsinoune.evenement.dao.EvenementRepository;
import fr.association.elmouhsinoune.evenement.entities.Evenement;
import fr.association.elmouhsinoune.evenement.proxy.EvenementProxy;
import fr.association.elmouhsinoune.evenement.service.contract.EvenementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class EvenementServiceImpl implements EvenementService {

    @Autowired
    private EvenementRepository repository;

    @Autowired
    private EvenementProxy proxy;

    @Override
    public List<Evenement> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Evenement> findByAdherentId(long adherentId) {

        List<Evenement> evenements = repository.findByAdherentId(adherentId);
        for (Evenement evenement : evenements) {
            Adherent adherent = proxy.getAdherentById(evenement.getAdherentId());
            evenement.setNomAdherent(adherent.getNom());
            evenement.setPrenomAdherent(adherent.getPrenom());
        }
        return evenements;
    }

    @Override
    public Page<Evenement> search(String natureEvenement, Long adherentId, int page, int size) {
        if (adherentId == 0) {
            return repository.searchWithoutAdherentId(natureEvenement, PageRequest.of(page, size));
        } else {
            return repository.search(natureEvenement, adherentId, PageRequest.of(page, size));
        }
    }

    @Override
    public Evenement findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public Evenement save(Evenement evenement) {
        return repository.save(evenement);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }
}
