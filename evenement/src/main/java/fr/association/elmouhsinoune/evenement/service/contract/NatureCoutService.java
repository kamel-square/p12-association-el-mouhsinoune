package fr.association.elmouhsinoune.evenement.service.contract;

import fr.association.elmouhsinoune.evenement.entities.NatureCout;

import java.util.List;

public interface NatureCoutService {

    List<NatureCout> findAll();
    NatureCout findById(long id);
    NatureCout save(NatureCout natureCout);
    void delete(long id);
}
