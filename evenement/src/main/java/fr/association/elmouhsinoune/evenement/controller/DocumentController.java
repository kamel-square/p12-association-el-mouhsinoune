package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.Document;
import fr.association.elmouhsinoune.evenement.service.contract.DocumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@RestController
public class DocumentController {

    @Autowired
    private DocumentService service;

    Logger log = LoggerFactory.getLogger(DocumentController.class);

    /**
     * Affiche la liste des documents.
     */
    @GetMapping(value = "/documents")
    public List<Document> documentList() {
        return service.findAll();
    }

    /**
     * Affiche un document de par son id.
     */
    @GetMapping(value = "/documents/{id}")
    public Document getDocumentById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Affiche la liste des documents de par l'evenementId.
     */
    @GetMapping(value = "/getDocumentsByEvenementId/{evenementId}")
    public List<Document> getDocumentsByEvenementId(@PathVariable("evenementId") long evenmentId) {
        return service.findByEvenementId(evenmentId);
    }

    /**
     * Affiche un fichier de par l' id du document.
     */
    @GetMapping(value = "/getDocument/{id}", produces = MediaType.APPLICATION_PDF_VALUE)
    public byte[] getDocument(@PathVariable("id") long id) throws Exception {
        Document document = service.findById(id);
        return Files.readAllBytes(Paths.get(System.getProperty("user.dir") + "/association/" + document.getDocument()));
    }

    /**
     * Ajoute un document.
     */
    @PostMapping(value = "/addDocument")
    public Document addDocument(@RequestBody Document document) {
        return service.save(document);
    }

    /**
     * Modifie un document.
     */
    @PutMapping(value = "/updateDocument")
    public Document updateDocument(@RequestBody Document document) {
        return service.save(document);
    }

    /**
     * Ajoute un fichier de par l' id du document.
     */
    @PostMapping(value = "/uploadFile/{id}")
    public Document addDocument(MultipartFile file, @PathVariable("id") long id) throws Exception {
        Document document = service.findById(id);
        document.setDocument(file.getOriginalFilename());
        Files.write(Paths.get(System.getProperty("user.dir") + "/association/" + document.getDocument()), file.getBytes());
        return service.save(document);
    }

    /**
     * Supprime un document.
     */
    @DeleteMapping(value = "/deleteDocument/{id}")
    public void deleteDocument(@PathVariable("id") long id) {
        service.delete(id);
    }
}
