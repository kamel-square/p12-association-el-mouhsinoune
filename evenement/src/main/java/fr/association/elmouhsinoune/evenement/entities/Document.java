package fr.association.elmouhsinoune.evenement.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Document implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String document;
    private Date date;
    @Column(name = "evenement_id")
    private long evenementId;
    @Column(name = "nature_document_id")
    private long natureDocumentId;

    @ManyToOne
    @JoinColumn(name = "evenement_id", referencedColumnName = "id", insertable= false, updatable= false)
    private Evenement evenement;

    @ManyToOne
    @JoinColumn(name = "nature_document_id", referencedColumnName = "id", insertable= false, updatable= false)
    private NatureDocument natureDocument;
}
