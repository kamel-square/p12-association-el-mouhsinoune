package fr.association.elmouhsinoune.evenement.controller;

import fr.association.elmouhsinoune.evenement.entities.Cout;
import fr.association.elmouhsinoune.evenement.service.contract.CoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CoutController {

    @Autowired
    private CoutService service;

    /**
     * Affiche la liste des coûts.
     */
    @GetMapping(value = "/couts")
    public List<Cout> coutList() {
        return service.findAll();
    }

    /**
     * Affiche la liste des coûts de par l'evenementId
     */
    @GetMapping(value = "/getCoutsByEvenementId/{evenementId}")
    public List<Cout> getCoutsByEvenementId(@PathVariable("evenementId") long evenementId) {
        return service.findByEvenementId(evenementId);
    }

    /**
     * Affiche un coût de par son id.
     */
    @GetMapping(value = "/couts/{id}")
    public Cout getCoutById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Ajoute un coût.
     */
    @PostMapping(value = "/saveCout")
    public Cout saveCout(@RequestBody Cout cout) {
        return service.save(cout);
    }

    /**
     * Modifie un coût.
     */
    @PutMapping(value = "/updateCout")
    public Cout updateCout(@RequestBody Cout cout) {
        return service.save(cout);
    }

    /**
     * Supprime un coût.
     */
    @DeleteMapping(value = "/deleteCout/{id}")
    public void deleteCout(@PathVariable("id") long id) {
        service.delete(id);
    }
}
