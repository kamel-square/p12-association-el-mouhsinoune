TRUNCATE TABLE moyen_paiement CASCADE;
INSERT INTO moyen_paiement (id, moyen_paiement)
VALUES
(1, 'Espèce'),
(2, 'Chèque'),
(3, 'Virement');

TRUNCATE TABLE nature_cout CASCADE;
INSERT INTO nature_cout (id, nature_cout)
VALUES
(1, 'Pompes funèbres'),
(2, 'Cercueil'),
(3, 'Enterrement'),
(4, 'Rapatriement'),
(5, 'Administratif');

TRUNCATE TABLE nature_document CASCADE;
INSERT INTO nature_document (id, nature_document)
VALUES
(1, 'Facture'),
(2, 'Acte de décès'),
(3, 'Devis');

TRUNCATE TABLE nature_evenement CASCADE;
INSERT INTO nature_evenement (id, nature_evenement)
VALUES
(1, 'Décès'),
(2, 'Rapatriement'),
(3, 'Enterrement');
