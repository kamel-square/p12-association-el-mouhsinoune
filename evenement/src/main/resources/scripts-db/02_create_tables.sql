DROP SEQUENCE IF EXISTS public.nature_cout_id_seq CASCADE;
CREATE SEQUENCE public.nature_cout_id_seq;

DROP TABLE IF EXISTS public.nature_cout CASCADE;
CREATE TABLE public.nature_cout (
                id BIGINT NOT NULL DEFAULT nextval('public.nature_cout_id_seq'),
                nature_cout VARCHAR NOT NULL,
                CONSTRAINT nature_cout_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.nature_cout_id_seq OWNED BY public.nature_cout.id;

DROP SEQUENCE IF EXISTS public.moyen_paiement_id_seq CASCADE;
CREATE SEQUENCE public.moyen_paiement_id_seq;

DROP TABLE IF EXISTS public.moyen_paiement CASCADE;
CREATE TABLE public.moyen_paiement (
                id BIGINT NOT NULL DEFAULT nextval('public.moyen_paiement_id_seq'),
                moyen_paiement VARCHAR(10) NOT NULL,
                CONSTRAINT moyen_paiement_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.moyen_paiement_id_seq OWNED BY public.moyen_paiement.id;

DROP SEQUENCE IF EXISTS public.cout_id_seq CASCADE;
CREATE SEQUENCE public.cout_id_seq;

DROP TABLE IF EXISTS public.cout CASCADE;
CREATE TABLE public.cout (
                id BIGINT NOT NULL DEFAULT nextval('public.cout_id_seq'),
                cout DOUBLE PRECISION NOT NULL,
                commentaire VARCHAR(1000) NOT NULL,
                date DATE NOT NULL,
                nature_cout_id BIGINT NOT NULL,
                moyen_paiement_id BIGINT NOT NULL,
				evenement_id BIGINT NOT NULL,
                CONSTRAINT cout_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.cout_id_seq OWNED BY public.cout.id;

DROP SEQUENCE IF EXISTS public.nature_document_id_seq CASCADE;
CREATE SEQUENCE public.nature_document_id_seq;

DROP TABLE IF EXISTS public.nature_document CASCADE;
CREATE TABLE public.nature_document (
                id BIGINT NOT NULL DEFAULT nextval('public.nature_document_id_seq'),
                nature_document VARCHAR NOT NULL,
                CONSTRAINT nature_document_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.nature_document_id_seq OWNED BY public.nature_document.id;

DROP SEQUENCE IF EXISTS public.nature_evenement_id_seq CASCADE;
CREATE SEQUENCE public.nature_evenement_id_seq;

DROP TABLE IF EXISTS public.nature_evenement CASCADE;
CREATE TABLE public.nature_evenement (
                id BIGINT NOT NULL DEFAULT nextval('public.nature_evenement_id_seq'),
                nature_evenement VARCHAR NOT NULL,
                CONSTRAINT nature_evenement_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.nature_evenement_id_seq OWNED BY public.nature_evenement.id;

DROP SEQUENCE IF EXISTS public.evenement_id_seq CASCADE;
CREATE SEQUENCE public.evenement_id_seq;

DROP TABLE IF EXISTS public.evenement CASCADE;
CREATE TABLE public.evenement (
                id BIGINT NOT NULL DEFAULT nextval('public.evenement_id_seq'),
                commentaire VARCHAR(1000),
				nature_evenement_id BIGINT NOT NULL,
                adherent_id BIGINT NOT NULL,
                CONSTRAINT evenement_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.evenement_id_seq OWNED BY public.evenement.id;

DROP SEQUENCE IF EXISTS public.document_id_seq CASCADE;
CREATE SEQUENCE public.document_id_seq;

DROP TABLE IF EXISTS public.document CASCADE;
CREATE TABLE public.document (
                id BIGINT NOT NULL DEFAULT nextval('public.document_id_seq'),
                document VARCHAR NOT NULL,
				date DATE NOT NULL,
                evenement_id BIGINT NOT NULL,
                nature_document_id BIGINT NOT NULL,
                CONSTRAINT document_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.document_id_seq OWNED BY public.document.id;


ALTER TABLE public.cout ADD CONSTRAINT nature_cout_cout_fk
FOREIGN KEY (nature_cout_id)
REFERENCES public.nature_cout (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cout ADD CONSTRAINT moyen_paiement_cout_fk
FOREIGN KEY (moyen_paiement_id)
REFERENCES public.moyen_paiement (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cout ADD CONSTRAINT evenement_cout_fk
FOREIGN KEY (evenement_id)
REFERENCES public.evenement (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.document ADD CONSTRAINT nature_document_document_fk
FOREIGN KEY (nature_document_id)
REFERENCES public.nature_document (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.evenement ADD CONSTRAINT nature_evenement_evenement_fk
FOREIGN KEY (nature_evenement_id)
REFERENCES public.nature_evenement (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.document ADD CONSTRAINT evenement_justificatif_fk
FOREIGN KEY (evenement_id)
REFERENCES public.evenement (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
