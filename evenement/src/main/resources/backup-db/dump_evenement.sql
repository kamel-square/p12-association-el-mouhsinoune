--
-- PostgreSQL database dump
--

-- Dumped from database version 13.0
-- Dumped by pg_dump version 13.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cout; Type: TABLE; Schema: public; Owner: association
--

CREATE TABLE public.cout (
    id bigint NOT NULL,
    cout double precision NOT NULL,
    commentaire character varying(1000),
    date date NOT NULL,
    nature_cout_id bigint NOT NULL,
    moyen_paiement_id bigint NOT NULL,
    evenement_id bigint NOT NULL
);


ALTER TABLE public.cout OWNER TO association;

--
-- Name: cout_id_seq; Type: SEQUENCE; Schema: public; Owner: association
--

CREATE SEQUENCE public.cout_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cout_id_seq OWNER TO association;

--
-- Name: cout_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: association
--

ALTER SEQUENCE public.cout_id_seq OWNED BY public.cout.id;


--
-- Name: document; Type: TABLE; Schema: public; Owner: association
--

CREATE TABLE public.document (
    id bigint NOT NULL,
    document character varying NOT NULL,
    evenement_id bigint NOT NULL,
    nature_document_id bigint NOT NULL,
    date timestamp without time zone
);


ALTER TABLE public.document OWNER TO association;

--
-- Name: document_id_seq; Type: SEQUENCE; Schema: public; Owner: association
--

CREATE SEQUENCE public.document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.document_id_seq OWNER TO association;

--
-- Name: document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: association
--

ALTER SEQUENCE public.document_id_seq OWNED BY public.document.id;


--
-- Name: evenement; Type: TABLE; Schema: public; Owner: association
--

CREATE TABLE public.evenement (
    id bigint NOT NULL,
    nature_evenement_id bigint NOT NULL,
    adherent_id bigint NOT NULL,
    membre_famille_id bigint,
    commentaire character varying(1000)
);


ALTER TABLE public.evenement OWNER TO association;

--
-- Name: evenement_id_seq; Type: SEQUENCE; Schema: public; Owner: association
--

CREATE SEQUENCE public.evenement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.evenement_id_seq OWNER TO association;

--
-- Name: evenement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: association
--

ALTER SEQUENCE public.evenement_id_seq OWNED BY public.evenement.id;


--
-- Name: moyen_paiement; Type: TABLE; Schema: public; Owner: association
--

CREATE TABLE public.moyen_paiement (
    id bigint NOT NULL,
    moyen_paiement character varying(255)
);


ALTER TABLE public.moyen_paiement OWNER TO association;

--
-- Name: moyen_paiement_id_seq; Type: SEQUENCE; Schema: public; Owner: association
--

CREATE SEQUENCE public.moyen_paiement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.moyen_paiement_id_seq OWNER TO association;

--
-- Name: moyen_paiement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: association
--

ALTER SEQUENCE public.moyen_paiement_id_seq OWNED BY public.moyen_paiement.id;


--
-- Name: nature_cout; Type: TABLE; Schema: public; Owner: association
--

CREATE TABLE public.nature_cout (
    id bigint NOT NULL,
    nature_cout character varying NOT NULL
);


ALTER TABLE public.nature_cout OWNER TO association;

--
-- Name: nature_cout_id_seq; Type: SEQUENCE; Schema: public; Owner: association
--

CREATE SEQUENCE public.nature_cout_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nature_cout_id_seq OWNER TO association;

--
-- Name: nature_cout_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: association
--

ALTER SEQUENCE public.nature_cout_id_seq OWNED BY public.nature_cout.id;


--
-- Name: nature_document; Type: TABLE; Schema: public; Owner: association
--

CREATE TABLE public.nature_document (
    id bigint NOT NULL,
    nature_document character varying NOT NULL
);


ALTER TABLE public.nature_document OWNER TO association;

--
-- Name: nature_document_id_seq; Type: SEQUENCE; Schema: public; Owner: association
--

CREATE SEQUENCE public.nature_document_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nature_document_id_seq OWNER TO association;

--
-- Name: nature_document_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: association
--

ALTER SEQUENCE public.nature_document_id_seq OWNED BY public.nature_document.id;


--
-- Name: nature_evenement; Type: TABLE; Schema: public; Owner: association
--

CREATE TABLE public.nature_evenement (
    id bigint NOT NULL,
    nature_evenement character varying NOT NULL
);


ALTER TABLE public.nature_evenement OWNER TO association;

--
-- Name: nature_evenement_id_seq; Type: SEQUENCE; Schema: public; Owner: association
--

CREATE SEQUENCE public.nature_evenement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nature_evenement_id_seq OWNER TO association;

--
-- Name: nature_evenement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: association
--

ALTER SEQUENCE public.nature_evenement_id_seq OWNED BY public.nature_evenement.id;


--
-- Name: cout id; Type: DEFAULT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.cout ALTER COLUMN id SET DEFAULT nextval('public.cout_id_seq'::regclass);


--
-- Name: document id; Type: DEFAULT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.document ALTER COLUMN id SET DEFAULT nextval('public.document_id_seq'::regclass);


--
-- Name: evenement id; Type: DEFAULT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.evenement ALTER COLUMN id SET DEFAULT nextval('public.evenement_id_seq'::regclass);


--
-- Name: moyen_paiement id; Type: DEFAULT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.moyen_paiement ALTER COLUMN id SET DEFAULT nextval('public.moyen_paiement_id_seq'::regclass);


--
-- Name: nature_cout id; Type: DEFAULT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.nature_cout ALTER COLUMN id SET DEFAULT nextval('public.nature_cout_id_seq'::regclass);


--
-- Name: nature_document id; Type: DEFAULT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.nature_document ALTER COLUMN id SET DEFAULT nextval('public.nature_document_id_seq'::regclass);


--
-- Name: nature_evenement id; Type: DEFAULT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.nature_evenement ALTER COLUMN id SET DEFAULT nextval('public.nature_evenement_id_seq'::regclass);


--
-- Data for Name: cout; Type: TABLE DATA; Schema: public; Owner: association
--

COPY public.cout (id, cout, commentaire, date, nature_cout_id, moyen_paiement_id, evenement_id) FROM stdin;
1	50.5	\N	2020-11-04	2	1	1
2	250	\N	2020-11-04	3	1	1
\.


--
-- Data for Name: document; Type: TABLE DATA; Schema: public; Owner: association
--

COPY public.document (id, document, evenement_id, nature_document_id, date) FROM stdin;
\.


--
-- Data for Name: evenement; Type: TABLE DATA; Schema: public; Owner: association
--

COPY public.evenement (id, nature_evenement_id, adherent_id, membre_famille_id, commentaire) FROM stdin;
1	1	3	\N	\N
7	3	4	\N	\N
2	2	3	\N	\N
\.


--
-- Data for Name: moyen_paiement; Type: TABLE DATA; Schema: public; Owner: association
--

COPY public.moyen_paiement (id, moyen_paiement) FROM stdin;
1	Espèce
2	Chèque
3	Virement
\.


--
-- Data for Name: nature_cout; Type: TABLE DATA; Schema: public; Owner: association
--

COPY public.nature_cout (id, nature_cout) FROM stdin;
1	Pompes funèbres
2	Cercueil
3	Enterrement
4	Rapatriement
5	Administratif
\.


--
-- Data for Name: nature_document; Type: TABLE DATA; Schema: public; Owner: association
--

COPY public.nature_document (id, nature_document) FROM stdin;
1	Facture
2	Acte de décès
\.


--
-- Data for Name: nature_evenement; Type: TABLE DATA; Schema: public; Owner: association
--

COPY public.nature_evenement (id, nature_evenement) FROM stdin;
1	Décès
2	Rapatriement
3	Enterrement
\.


--
-- Name: cout_id_seq; Type: SEQUENCE SET; Schema: public; Owner: association
--

SELECT pg_catalog.setval('public.cout_id_seq', 1, false);


--
-- Name: document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: association
--

SELECT pg_catalog.setval('public.document_id_seq', 1, false);


--
-- Name: evenement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: association
--

SELECT pg_catalog.setval('public.evenement_id_seq', 7, true);


--
-- Name: moyen_paiement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: association
--

SELECT pg_catalog.setval('public.moyen_paiement_id_seq', 1, false);


--
-- Name: nature_cout_id_seq; Type: SEQUENCE SET; Schema: public; Owner: association
--

SELECT pg_catalog.setval('public.nature_cout_id_seq', 1, false);


--
-- Name: nature_document_id_seq; Type: SEQUENCE SET; Schema: public; Owner: association
--

SELECT pg_catalog.setval('public.nature_document_id_seq', 1, false);


--
-- Name: nature_evenement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: association
--

SELECT pg_catalog.setval('public.nature_evenement_id_seq', 1, false);


--
-- Name: cout cout_pk; Type: CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.cout
    ADD CONSTRAINT cout_pk PRIMARY KEY (id);


--
-- Name: document document_pk; Type: CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT document_pk PRIMARY KEY (id);


--
-- Name: evenement evenement_pk; Type: CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.evenement
    ADD CONSTRAINT evenement_pk PRIMARY KEY (id);


--
-- Name: moyen_paiement moyen_paiement_pkey; Type: CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.moyen_paiement
    ADD CONSTRAINT moyen_paiement_pkey PRIMARY KEY (id);


--
-- Name: nature_cout nature_cout_pk; Type: CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.nature_cout
    ADD CONSTRAINT nature_cout_pk PRIMARY KEY (id);


--
-- Name: nature_document nature_document_pk; Type: CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.nature_document
    ADD CONSTRAINT nature_document_pk PRIMARY KEY (id);


--
-- Name: nature_evenement nature_evenement_pk; Type: CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.nature_evenement
    ADD CONSTRAINT nature_evenement_pk PRIMARY KEY (id);


--
-- Name: cout evenement_cout_fk; Type: FK CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.cout
    ADD CONSTRAINT evenement_cout_fk FOREIGN KEY (evenement_id) REFERENCES public.evenement(id);


--
-- Name: document evenement_justificatif_fk; Type: FK CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT evenement_justificatif_fk FOREIGN KEY (evenement_id) REFERENCES public.evenement(id);


--
-- Name: cout fktrv4bbct744adqyvapbkpy6do; Type: FK CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.cout
    ADD CONSTRAINT fktrv4bbct744adqyvapbkpy6do FOREIGN KEY (moyen_paiement_id) REFERENCES public.moyen_paiement(id);


--
-- Name: cout nature_cout_cout_fk; Type: FK CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.cout
    ADD CONSTRAINT nature_cout_cout_fk FOREIGN KEY (nature_cout_id) REFERENCES public.nature_cout(id);


--
-- Name: document nature_document_document_fk; Type: FK CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.document
    ADD CONSTRAINT nature_document_document_fk FOREIGN KEY (nature_document_id) REFERENCES public.nature_document(id);


--
-- Name: evenement nature_evenement_evenement_fk; Type: FK CONSTRAINT; Schema: public; Owner: association
--

ALTER TABLE ONLY public.evenement
    ADD CONSTRAINT nature_evenement_evenement_fk FOREIGN KEY (nature_evenement_id) REFERENCES public.nature_evenement(id);


--
-- PostgreSQL database dump complete
--

