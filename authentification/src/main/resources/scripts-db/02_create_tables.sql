DROP SEQUENCE IF EXISTS public.utilisateur_id_seq CASCADE;
CREATE SEQUENCE public.utilisateur_id_seq;

DROP TABLE IF EXISTS public.utilisateur CASCADE;
CREATE TABLE public.utilisateur (
                id BIGINT NOT NULL DEFAULT nextval('public.utilisateur_id_seq'),
                pseudo VARCHAR(50) UNIQUE NOT NULL,
                password VARCHAR NOT NULL,
                statut VARCHAR(15) NOT NULL,
                actif BOOLEAN NOT NULL,
                CONSTRAINT utilisateur_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.utilisateur_id_seq OWNED BY public.utilisateur.id;
