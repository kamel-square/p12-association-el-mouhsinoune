TRUNCATE TABLE utilisateur CASCADE;
INSERT INTO utilisateur (id, pseudo, password, statut, actif)
VALUES
(1, 'ponpon', '$2y$10$cGKWsdqaQVYojwSWFjfbBO3eEFnIryiJY1cXYK88U/8/2dsoXiCeG', 'Responsable', true),
(2, 'memed', '$2y$10$hW/HFbkMH4j/7YLPnbgv8efJaaw7fk4IzBX4tTTpBjd4TB/unGaou', 'Adhérent', true),
(3, 'mouss', '$2y$10$VUF3yivWtp2Z/mN1/kxQOOQOGtwo2dYKAB3bIeBnJWnj55sx1abEq', 'Adhérent', true),
(4, 'nana', '$2y$10$uOWfSQpg.eH8PelipfVuAuKT6TcjS7l6cd8vfTqfitjq2Y3VHJ3aq', 'Adhérent', true);
