package fr.association.elmouhsinoune.authentification.configuration;

public class Constante {
    public static final String SECRET = "$2y$10$J93RZeFFH.hzzPHmVIsXGODDlv9LzB6cpOjPyziVa9ZodTgUL3VYO";
}
