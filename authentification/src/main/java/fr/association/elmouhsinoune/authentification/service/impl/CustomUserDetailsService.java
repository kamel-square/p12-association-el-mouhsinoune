package fr.association.elmouhsinoune.authentification.service.impl;

import fr.association.elmouhsinoune.authentification.dao.UtilisateurRepository;
import fr.association.elmouhsinoune.authentification.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UtilisateurRepository repository;

    @Override
    public UserDetails loadUserByUsername(String pseudo) throws UsernameNotFoundException {
        Utilisateur utilisateur = repository.findByPseudo(pseudo);
        return new org.springframework.security.core.userdetails.User(utilisateur.getPseudo(), utilisateur.getPassword(), new ArrayList<>());
    }
}
