package fr.association.elmouhsinoune.authentification.controller;

import fr.association.elmouhsinoune.authentification.entities.AuthRequest;
import fr.association.elmouhsinoune.authentification.entities.Utilisateur;
import fr.association.elmouhsinoune.authentification.service.contract.UtilisateurService;
import fr.association.elmouhsinoune.authentification.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UtilisateurController {

    @Autowired
    private UtilisateurService service;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    /**
     * Affiche la liste des utilisateurs.
     */
    @GetMapping(value = "/listeUtilisateurs")
    public List<Utilisateur> listeUtilisateurs() {
        return service.findAll();
    }

    /**
     * Cherche un utilisateur avec son ID.
     */
    @GetMapping(value = "/utilisateur/{id}")
    public Utilisateur getUtilisateur(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Cherche un utilisateur avec son pseudo.
     */
    @GetMapping(value = "/utilisateurByPseudo/{pseudo}")
    public Utilisateur getUtilisateurWithPseudo(@PathVariable("pseudo") String pseudo) {
        return service.findByPseudo(pseudo);
    }

    /**
     * Création du token.
     */
    @PostMapping("/authenticate")
    public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authRequest.getPseudo(), authRequest.getPassword())
            );
        } catch (Exception e) {
            throw new Exception("invalid pseudo/password");
        }
        return jwtUtil.generateToken(authRequest.getPseudo());
    }

    /**
     * Enregistre un utilisateur et encode son mot de passe.
     */
    @PostMapping(value = "/saveUtilisateur")
    public Utilisateur saveUtilisateur(@RequestBody Utilisateur utilisateur) {
        return service.save(utilisateur);
    }

    /**
     * Suppression d'un utilisateur de par son id.
     */
    @DeleteMapping(value = "/deleteUtilisateur/{id}")
    public void deleteUtilisateur(@PathVariable("id") long id) {
        service.delete(id);
    }

}
