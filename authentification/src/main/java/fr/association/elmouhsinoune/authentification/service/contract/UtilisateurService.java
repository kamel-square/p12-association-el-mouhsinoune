package fr.association.elmouhsinoune.authentification.service.contract;

import fr.association.elmouhsinoune.authentification.entities.Utilisateur;

import java.util.List;

public interface UtilisateurService {

    List<Utilisateur> findAll();
    Utilisateur findById(long id);
    Utilisateur findByPseudo(String pseudo);
    void checkIfPseudoExists(String pseudoInDb, String newPseudo) throws Exception;
    Utilisateur save(Utilisateur utilisateur);
    void delete(long id);

}
