package fr.association.elmouhsinoune.authentification.dao;

import fr.association.elmouhsinoune.authentification.entities.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

    Utilisateur findByPseudo(String pseudo);
}
