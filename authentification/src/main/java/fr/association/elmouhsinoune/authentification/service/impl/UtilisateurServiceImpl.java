package fr.association.elmouhsinoune.authentification.service.impl;

import fr.association.elmouhsinoune.authentification.dao.UtilisateurRepository;
import fr.association.elmouhsinoune.authentification.entities.Utilisateur;
import fr.association.elmouhsinoune.authentification.service.contract.UtilisateurService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UtilisateurServiceImpl implements UtilisateurService {

    @Autowired
    private UtilisateurRepository repository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private Logger log = LoggerFactory.getLogger(UtilisateurServiceImpl.class);

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public List<Utilisateur> findAll() {
        return repository.findAll();
    }

    @Override
    public Utilisateur findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public Utilisateur findByPseudo(String pseudo) {
        return repository.findByPseudo(pseudo);
    }

    public void checkIfPseudoExists(String pseudoInDb, String newPseudo) throws Exception {
        if (pseudoInDb == newPseudo) {
            log.info("Le pseudo " + newPseudo + " existe déjà.");
            throw new Exception("Le pseudo non disponible...");
        } else {
            log.info("Le pseudo " + newPseudo + " est disponible.");
        }
    }

    @Override
    public Utilisateur save(Utilisateur utilisateur) {
        if (repository.findByPseudo(utilisateur.getPseudo()) != null) {
            Utilisateur utilisateurInDb = repository.findByPseudo(utilisateur.getPseudo());
            try {
                checkIfPseudoExists(utilisateurInDb.getPseudo(), utilisateur.getPseudo());
            } catch (Exception e) {
                log.info(e.getMessage());
            }
        } else {
            try {
                checkIfPseudoExists("", utilisateur.getPseudo());
            } catch (Exception e) {
                log.info(e.getMessage());
            }
        }
        String passEncoder = passwordEncoder.encode(utilisateur.getPassword());
        utilisateur.setPassword(passEncoder);
        return repository.save(utilisateur);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }
}
