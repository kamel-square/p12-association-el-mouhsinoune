package fr.association.elmouhsinoune.batch.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Cotisation {

    private Long id;
    private long adherentId;
    private int montant;
    private Date date;
    private int periode;
    private long moyenPaiementId;
}
