package fr.association.elmouhsinoune.batch.job;

import fr.association.elmouhsinoune.batch.beans.Adherent;
import fr.association.elmouhsinoune.batch.beans.Cotisation;
import fr.association.elmouhsinoune.batch.proxy.BatchProxy;
import fr.association.elmouhsinoune.batch.service.SimpleEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Component
@ConditionalOnExpression("'${scheduler.enabled}'=='true'")
public class Task {

    private Logger log = LoggerFactory.getLogger(Task.class);

    @Autowired
    private BatchProxy proxy;

    @Autowired
    private SimpleEmailService emailService;

    /**
     * Vérifie, tous les 15 du mois, s'il y a des adhérents qui sont en retard d'au moins 3 mois dans
     * le paiement de leurs cotisations puis cela leur envoie automatiquement un mail de relance.
     */
    @Scheduled(cron = "0 0 12 15 * ?", zone = "Europe/Paris")
    public void executeTask() {

        List<Cotisation> cotisationList = proxy.cotisationList();

        Date date = new Date();

        String pattern = "dd/MM/yyyy";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);


        for (Cotisation cotisation : cotisationList) {

            Adherent adherent = proxy.getAdherentById(cotisation.getAdherentId());

            List<Cotisation> cotisationAdherent = proxy.getCotisationsByAdherentId(cotisation.getAdherentId());

            int periodeByAdherent = cotisationAdherent.get(cotisationAdherent.size() - 1).getPeriode();
            Date dateOfLastCotisationByAdherent = cotisationAdherent.get(cotisationAdherent.size() - 1).getDate();

            int periodePassee = periodeByAdherent + 3;

            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(dateOfLastCotisationByAdherent);
            calendar.add(GregorianCalendar.MONTH + 1, +periodePassee);

            log.info("Période passée = " + calendar);
            log.info("Mois = " + calendar.get(GregorianCalendar.MONTH + 1));
            log.info("Mois + 3 = " + calendar.get(GregorianCalendar.MONTH + 1 + periodePassee));

            Date datePassee = calendar.getTime();

            Long today = date.getTime();
            Long datePasseeLong = datePassee.getTime();

            if (today >= datePasseeLong) {
                log.info("Cela fait 3 mois que vous n'avez pas réglé votre cotisation...\n" +
                        "Vous devez donc régler votre cotisation au plus vite si vous ne voulez pas être radié.");

                    String dateString = simpleDateFormat.format(dateOfLastCotisationByAdherent);
                    String civilite = "";
                    String destinataire = adherent.getEmail();
                    String objet = "Rappel, retard sur le paiement de la cotisation !";

                    if (adherent.getGenreId() == 1) {
                        civilite = "Mr";
                    } else {
                        civilite = "Mme";
                    }

                    String message = "Bonjour " + civilite + " " + adherent.getNom() + " " + adherent.getPrenom() + ", " +
                            "\n\nVous avez 3 mois de retard sur le paiement de votre cotisation depuis le " +
                            dateString + "." +
                            "\nMerci de la régler au plus vite pour ne pas être radié." +
                            "\n\nService adhérent de l'association";

                    // envoie du mail
                    log.info("****************************************************************************************");
                    log.info("Email envoyé a: " + destinataire);
                    log.info("****************************************************************************************");
                    emailService.sendSimpleEmail(destinataire, objet, message);
            } else {
                log.info("****************************************************************************************");
                log.info("Il n'y a aucun email de rappel à envoyer.");
                log.info("****************************************************************************************");
            }

        }
    }

}
