package fr.association.elmouhsinoune.batch.proxy;

import fr.association.elmouhsinoune.batch.beans.Adherent;
import fr.association.elmouhsinoune.batch.beans.Cotisation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "gateway-server")
public interface BatchProxy {

    /**
     * Récupère l'adhérent de par son id auprès du microservice adherent.
     */
    @GetMapping(value = "/adherent/adherent/{id}")
    Adherent getAdherentById(@PathVariable("id") long id);

    /**
     * Récupère la liste des cotisations auprès du microservice cotisation.
     */
    @GetMapping(value = "/cotisation/cotisations")
    List<Cotisation> cotisationList();

    /**
     * Récupère la liste des cotisations de par l'adherentId auprès du microservice cotisation.
     */
    @GetMapping(value = "/cotisation/cotisationsByAdherentId/{adherentId}")
    List<Cotisation> getCotisationsByAdherentId(@PathVariable("adherentId") long adherentId);

}
