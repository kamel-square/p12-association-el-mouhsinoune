package fr.association.elmouhsinoune.batch.beans;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Adherent {

    private Long id;
    private String numAdherent;
    private String nom;
    private String prenom;
    private String email;
    private String adresse;
    private String situation;
    private String telephone;
    private long paysId;
    private long utilisateurId;
    private long genreId;

}
