DROP SEQUENCE IF EXISTS public.cotisation_id_seq CASCADE;
CREATE SEQUENCE public.cotisation_id_seq;

DROP TABLE IF EXISTS public.cotisation CASCADE;
CREATE TABLE public.cotisation (
                id BIGINT NOT NULL DEFAULT nextval('public.cotisation_id_seq'),
                adherent_id BIGINT NOT NULL,
                montant DOUBLE PRECISION NOT NULL,
                date DATE NOT NULL,
                periode INTEGER NOT NULL,
                moyen_paiement_id BIGINT NOT NULL,
                CONSTRAINT cotisation_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.cotisation_id_seq OWNED BY public.cotisation.id;