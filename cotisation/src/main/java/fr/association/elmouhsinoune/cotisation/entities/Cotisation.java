package fr.association.elmouhsinoune.cotisation.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Cotisation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "adherent_id")
    private long adherentId;
    private int montant;
    private Date date;
    private int periode;
    @Column(name = "moyen_paiement_id")
    private long moyenPaiementId;
}
