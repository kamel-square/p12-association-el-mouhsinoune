package fr.association.elmouhsinoune.cotisation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CotisationApplication {

	public static void main(String[] args) {
		SpringApplication.run(CotisationApplication.class, args);
	}

}
