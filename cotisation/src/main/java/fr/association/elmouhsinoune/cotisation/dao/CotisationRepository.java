package fr.association.elmouhsinoune.cotisation.dao;


import fr.association.elmouhsinoune.cotisation.entities.Cotisation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Date;
import java.util.List;

@RepositoryRestResource
public interface CotisationRepository extends JpaRepository<Cotisation, Long> {

    List<Cotisation> findByAdherentId(long adherentId);

    @Query("select c from Cotisation c where c.adherentId = :adherentId and c.moyenPaiementId = :moyenPaiementId")
    Page<Cotisation> searchByKeyword(@Param("adherentId") long adherentId,
                                     @Param("moyenPaiementId") long moyenPaiementId, Pageable pageable);

    Page<Cotisation> findByDate(Date date, Pageable pageable);
    Page<Cotisation> findByAdherentId(Long adherentId, Pageable pageable);
    Page<Cotisation> findByMoyenPaiementId(Long moyenPaiementId, Pageable pageable);
    Page<Cotisation> findByDateAndAdherentId(Date date, Long adherentId, Pageable pageable);
    Page<Cotisation> findByDateAndMoyenPaiementId(Date date, Long moyenPaiementId, Pageable pageable);
    Page<Cotisation> findByAdherentIdAndMoyenPaiementId(Long adherentId, Long moyenPaiementId, Pageable pageable);
    Page<Cotisation> findByDateAndAdherentIdAndMoyenPaiementId(Date date, Long adherentId, Long moyenPaiementId, Pageable pageable);
}
