package fr.association.elmouhsinoune.cotisation.service.impl;

import fr.association.elmouhsinoune.cotisation.dao.CotisationRepository;
import fr.association.elmouhsinoune.cotisation.entities.Cotisation;
import fr.association.elmouhsinoune.cotisation.service.contract.CotisationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Service
public class CotisationServiceImpl implements CotisationService {

    @Autowired
    private CotisationRepository repository;

    private Logger log = LoggerFactory.getLogger(CotisationServiceImpl.class);

    @Override
    public List<Cotisation> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Cotisation> findByAdherentId(long adherentId) {
        return repository.findByAdherentId(adherentId);
    }

    @Override
    public Page<Cotisation> search(String dateString, Long adherentId, Long moyenPaiementId, int page, int size) throws ParseException {

        Page<Cotisation> resultat = repository.findAll(PageRequest.of(page, size));

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        if (adherentId != null && adherentId == 0) {
            adherentId = null;
        }
        if (moyenPaiementId != null && moyenPaiementId == 0) {
            moyenPaiementId = null;
        }
        if (dateString != null && dateString == "") {
            dateString = null;
        }

        if (dateString != null) {
            Date date = format.parse(dateString);
            log.info("Date : " + date);
            resultat = repository.findByDate(date, PageRequest.of(page, size));
        }
        if (adherentId != null) {
            log.info("Adhérent : " + adherentId);
            resultat = repository.findByAdherentId(adherentId, PageRequest.of(page, size));
        }
        if (moyenPaiementId != null) {
            log.info("Moyen : " + moyenPaiementId);
            resultat = repository.findByMoyenPaiementId(moyenPaiementId, PageRequest.of(page, size));
        }
        if (dateString != null && adherentId != null) {
            Date date = format.parse(dateString);
            log.info("Date : " + date + " - Adhérent : " + adherentId);
            resultat = repository.findByDateAndAdherentId(date, adherentId, PageRequest.of(page, size));
        }
        if (dateString != null && moyenPaiementId != null) {
            Date date = format.parse(dateString);
            log.info("Date : " + date + " - Moyen : " + moyenPaiementId);
            resultat = repository.findByDateAndMoyenPaiementId(date, moyenPaiementId, PageRequest.of(page, size));
        }
        if (adherentId != null && moyenPaiementId != null) {
            log.info("Adhérent : " + adherentId + " - Moyen : " + moyenPaiementId);
            resultat = repository.findByAdherentIdAndMoyenPaiementId(adherentId, moyenPaiementId, PageRequest.of(page, size));
        }
        if (dateString != null && adherentId != null && moyenPaiementId != null) {
            Date date = format.parse(dateString);
            log.info("Date : " + date + " - Adhérent : " + adherentId + " - Moyen : " + moyenPaiementId);
            resultat = repository.findByDateAndAdherentIdAndMoyenPaiementId(date, adherentId, moyenPaiementId, PageRequest.of(page, size));
        }
        return resultat;
    }

    @Override
    public Cotisation findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public Cotisation save(Cotisation cotisation) {
        return repository.save(cotisation);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }
}
