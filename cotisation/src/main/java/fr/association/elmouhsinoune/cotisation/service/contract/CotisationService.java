package fr.association.elmouhsinoune.cotisation.service.contract;

import fr.association.elmouhsinoune.cotisation.entities.Cotisation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface CotisationService {

    List<Cotisation> findAll();
    List<Cotisation> findByAdherentId(long adherentId);
    Page<Cotisation> search(String date, Long adherentId, Long moyenPaiementId, int page, int size) throws ParseException;
    Cotisation findById(long id);
    Cotisation save(Cotisation cotisation);
    void delete(long id);
}
