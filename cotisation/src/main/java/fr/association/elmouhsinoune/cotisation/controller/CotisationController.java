package fr.association.elmouhsinoune.cotisation.controller;

import fr.association.elmouhsinoune.cotisation.entities.Cotisation;
import fr.association.elmouhsinoune.cotisation.service.contract.CotisationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

@RestController
public class CotisationController {

    @Autowired
    private CotisationService service;

    /**
     * Affiche la liste des cotisations.
     */
    @GetMapping(value = "/cotisations")
    public List<Cotisation> cotisationList() {
        return service.findAll();
    }

    /**
     * Affiche la liste des cotisations de par l'adherentId.
     */
    @GetMapping(value = "/cotisationsByAdherentId/{adherentId}")
    public List<Cotisation> getCotisationsByAdherentId(@PathVariable("adherentId") long adherentId) {
        return service.findByAdherentId(adherentId);
    }

    /**
     * Affiche le résultat de la recherche.
     * @param date
     * @param adherentId
     * @param moyenPaiementId
     * @param page
     * @param size
     * @throws ParseException
     */
    @GetMapping(value = "/searchCotisations")
    public Page<Cotisation> searchByKeyword(String date, Long adherentId, Long moyenPaiementId,
                                            @RequestParam(name = "page", defaultValue = "0") int page,
                                            @RequestParam(name = "size", defaultValue = "5") int size) throws ParseException {

        return service.search(date, adherentId, moyenPaiementId, page, size);
    }

    /**
     * Affiche la cotisation de par son id.
     */
    @GetMapping(value = "/cotisations/{id}")
    public Cotisation getCotisationById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Ajoute une cotisation.
     */
    @PostMapping(value = "/addCotisation")
    public Cotisation addCotisation(@RequestBody Cotisation cotisation) {
        return service.save(cotisation);
    }

    /**
     * Modifie une cotisation.
     */
    @PutMapping(value = "/updateCotisation")
    public Cotisation updateCotisation(@RequestBody Cotisation cotisation) {
        return service.save(cotisation);
    }

    /**
     * Supprime une cotisation.
     */
    @DeleteMapping(value = "/deleteCotisation/{id}")
    public void deleteCotisation(@PathVariable("id") long id) {
        service.delete(id);
    }

}
