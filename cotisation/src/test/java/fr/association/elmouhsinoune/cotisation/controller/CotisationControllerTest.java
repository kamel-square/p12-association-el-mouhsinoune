package fr.association.elmouhsinoune.cotisation.controller;

import fr.association.elmouhsinoune.cotisation.entities.Cotisation;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(CotisationController.class)
@AutoConfigureMockMvc
public class CotisationControllerTest {

    @MockBean
    private CotisationController controller;

    @Autowired
    private MockMvc mockMvc;

    private static Cotisation cotisation;

    @BeforeAll
    public static void init() {
        cotisation = new Cotisation();
        cotisation.setMontant(15);
        cotisation.setPeriode(1);
        cotisation.setDate(new Date());
        cotisation.setAdherentId(1);
        cotisation.setMoyenPaiementId(1);
    }

    @Test
    void getCotisationById() throws Exception {
        // Given
        when(controller.getCotisationById(anyLong())).thenReturn(cotisation);

        // When
        RequestBuilder builder = MockMvcRequestBuilders.get("/cotisations/50");

        // Then
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.jsonPath("$.adherentId").value(Long.valueOf(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.montant").value(15))
                .andExpect(status().isOk());

        verify(controller).getCotisationById(anyLong());

    }
}
