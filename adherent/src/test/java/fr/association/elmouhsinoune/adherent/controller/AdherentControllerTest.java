package fr.association.elmouhsinoune.adherent.controller;

import fr.association.elmouhsinoune.adherent.controller.AdherentController;
import fr.association.elmouhsinoune.adherent.dao.AdherentRepository;
import fr.association.elmouhsinoune.adherent.entities.Adherent;
import fr.association.elmouhsinoune.adherent.service.contract.AdherentService;
import fr.association.elmouhsinoune.adherent.service.impl.AdherentServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AdherentController.class)
@AutoConfigureMockMvc
public class AdherentControllerTest {

    @MockBean
    private AdherentController controller;

    @Autowired
    private MockMvc mockMvc;

    private static Adherent adherent;

    @BeforeAll
    public static void init() {
        adherent = new Adherent();
        adherent.setNumAdherent("45210");
        adherent.setNom("Dupont");
        adherent.setPrenom("Jean");
        adherent.setAdresse("Vitry");
        adherent.setEmail("test@test.com");
        adherent.setGenreId(1);
        adherent.setPaysId(1);
        adherent.setSituation("Marié");
    }

    @Test
    void getAdherentById() throws Exception {
        // Given
        when(controller.getAdherentById(anyLong())).thenReturn(adherent);

        // When
        RequestBuilder builder = MockMvcRequestBuilders.get("/adherent/50");

        // Then
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.jsonPath("$.genreId").value(Long.valueOf(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.paysId").value(Long.valueOf(1)))
                .andExpect(status().isOk());

        verify(controller).getAdherentById(anyLong());

    }

}
