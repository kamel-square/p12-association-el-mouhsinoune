package fr.association.elmouhsinoune.adherent.service;

import fr.association.elmouhsinoune.adherent.dao.AdherentRepository;
import fr.association.elmouhsinoune.adherent.entities.Adherent;
import fr.association.elmouhsinoune.adherent.service.contract.AdherentService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AdherentUnitTest {

    @Mock
    private AdherentRepository repository;

    private AdherentService service;
    private static Adherent adherent;

    @BeforeEach
    public void initMock() {
        repository = mock(AdherentRepository.class, RETURNS_DEEP_STUBS);
        service = mock(AdherentService.class, RETURNS_DEEP_STUBS);
    }

    @BeforeAll
    public static void init() {
        adherent = new Adherent();
        adherent.setNumAdherent("45210");
        adherent.setNom("Dupont");
        adherent.setPrenom("Jean");
        adherent.setAdresse("Vitry");
        adherent.setEmail("test@test.com");
        adherent.setGenreId(1);
        adherent.setPaysId(1);
        adherent.setSituation("Marié");
        adherent.setUtilisateurId(1);
    }

    @Test
    @DisplayName("Donne un adhérent, vérifie si l'email est disponible et retourne que l'email est disponible.")
    public void givenAdherent_whenEmailIsAvailable_thenANewAdherent() {
        // GIVEN
        Adherent newAdherent = new Adherent();
        newAdherent.setEmail("new@adherent.com");
        when(repository.findByEmail(newAdherent.getEmail())).thenReturn(null);

        // WHEN
        service.checkIfEmailExists(newAdherent.getEmail(), adherent.getEmail());

        // THEN
        assertThat(adherent.getEmail()).isNotEqualTo(newAdherent.getEmail());
    }

    @Test
    @DisplayName("Donne un adhérent, vérifie si l'email est disponible et retourne que l'email n'est pas disponible.")
    public void givenAdherent_whenEmailIsNotAvailable_thenAException() {
        // GIVEN
        Adherent newAdherent = new Adherent();
        newAdherent.setEmail("test@test.com");
        when(repository.findByEmail(newAdherent.getEmail())).thenReturn(newAdherent);

        // WHEN
        service.checkIfEmailExists(newAdherent.getEmail(), adherent.getEmail());

        // THEN
        assertThat(newAdherent.getEmail()).isEqualTo(adherent.getEmail());
    }

    @Test
    @DisplayName("Donne un adhérent, vérifie si l'utilisateurId est utilisé et retourne que l'utilisateurId est utilisé.")
    public void givenAdherent_whenUtilisateurIdIsUsed_thenAException() {
        // GIVEN
        Adherent newAdherent = new Adherent();
        newAdherent.setUtilisateurId(1);
        when(repository.findByUtilisateurId(newAdherent.getUtilisateurId())).thenReturn(null);

        // WHEN
        service.checkIfUtilisateurIdExists(newAdherent.getUtilisateurId(), adherent.getUtilisateurId());

        // THEN
        assertThat(newAdherent.getUtilisateurId()).isEqualTo(adherent.getUtilisateurId());
    }

    @Test
    @DisplayName("Donne un adhérent, vérifie si l'utilisateurId est utilisé et retourne que l'utilisateurId n'est pas utilisé.")
    public void givenAdherent_whenUtilisateurIdIsNotUsed_thenANewAdherent() {
        // GIVEN
        Adherent newAdherent = new Adherent();
        newAdherent.setUtilisateurId(2);
        when(repository.findByUtilisateurId(newAdherent.getUtilisateurId())).thenReturn(newAdherent);

        // WHEN
        service.checkIfUtilisateurIdExists(newAdherent.getUtilisateurId(), adherent.getUtilisateurId());

        // THEN
        assertThat(newAdherent.getUtilisateurId()).isNotEqualTo(adherent.getUtilisateurId());
    }
}
