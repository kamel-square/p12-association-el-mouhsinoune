package fr.association.elmouhsinoune.adherent.controller;

import fr.association.elmouhsinoune.adherent.entities.MembreFamille;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MembreFamilleController.class)
@AutoConfigureMockMvc
public class MembreFamilleControllerTest {

    @MockBean
    private MembreFamilleController controller;

    @Autowired
    private MockMvc mockMvc;

    private static MembreFamille membreFamille;

    @BeforeAll
    public static void init() {
        membreFamille = new MembreFamille();
        membreFamille.setNom("Test");
        membreFamille.setPrenom("Test");
        membreFamille.setAdherentId(1);
        membreFamille.setLienParenteId(1);
    }

    @Test
    void getMembreFamilleById() throws Exception {
        // Given
        when(controller.getMembreFamilleById(anyLong())).thenReturn(membreFamille);

        // When
        RequestBuilder builder = MockMvcRequestBuilders.get("/membreFamilles/50");

        // Then
        mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.jsonPath("$.adherentId").value(Long.valueOf(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nom").value("Test"))
                .andExpect(status().isOk());

        verify(controller).getMembreFamilleById(anyLong());

    }
}
