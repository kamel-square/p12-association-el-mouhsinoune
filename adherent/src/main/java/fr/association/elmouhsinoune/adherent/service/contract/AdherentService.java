package fr.association.elmouhsinoune.adherent.service.contract;

import fr.association.elmouhsinoune.adherent.entities.Adherent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AdherentService {

    List<Adherent> findAll();
    Page<Adherent> search(String numAdherent, String nom, String prenom, int page, int size);
    Adherent findById(long id);
    List<Adherent> findByNom(String nom);
    Adherent findByEmail(String email);
    Adherent findByUtilisateurId(long utilisateurId);
    void checkIfEmailExists(String emailInDb, String newEmail);
    void checkIfUtilisateurIdExists(Long utilisateurIdInAdherent, Long newUtilisateurId);
    Adherent save(Adherent adherent, boolean update);
    void delete(long id);
}
