package fr.association.elmouhsinoune.adherent.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class MembreFamille implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String prenom;
    @Column(name = "adherent_id")
    private long adherentId;
    @Column(name = "lien_parente_id")
    private long lienParenteId;

    @ManyToOne
    @JoinColumn(name = "lien_parente_id", referencedColumnName = "id", insertable= false, updatable= false)
    private LienParente lien;

    @ManyToOne
    @JoinColumn(name = "adherent_id", referencedColumnName = "id", insertable= false, updatable= false)
    private Adherent adherent;

}
