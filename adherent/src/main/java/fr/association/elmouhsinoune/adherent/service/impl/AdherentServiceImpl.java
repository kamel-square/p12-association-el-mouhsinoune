package fr.association.elmouhsinoune.adherent.service.impl;

import fr.association.elmouhsinoune.adherent.dao.AdherentRepository;
import fr.association.elmouhsinoune.adherent.entities.Adherent;
import fr.association.elmouhsinoune.adherent.exception.FunctionalException;
import fr.association.elmouhsinoune.adherent.service.contract.AdherentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdherentServiceImpl implements AdherentService {

    @Autowired
    private AdherentRepository repository;

    Logger log = LoggerFactory.getLogger(AdherentServiceImpl.class);

    @Override
    public List<Adherent> findAll() {
        return repository.findAll();
    }

    @Override
    public Page<Adherent> search(String numAdherent, String nom, String prenom, int page, int size) {
        return repository.search(numAdherent, nom, prenom, PageRequest.of(page, size));
    }

    @Override
    public Adherent findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public List<Adherent> findByNom(String nom) {
        return repository.findByNom(nom);
    }

    @Override
    public Adherent findByEmail(String email) {
        return repository.findByEmail(email);
    }

    @Override
    public Adherent findByUtilisateurId(long utilisateurId) {
        return repository.findByUtilisateurId(utilisateurId);
    }

    /**
     * Vérifie si l'adresse email existe en base de données, si elle existe cela déclenche une exception.
     * @param emailInDb
     * @param newEmail
     */
    public void checkIfEmailExists(String emailInDb, String newEmail) {
        if (emailInDb.equals(newEmail)) {
            log.info("L'adresse email : " + newEmail + " existe déjà.");
            throw new FunctionalException("Email déjà existant...");
        } else {
            log.info("L'adresse email : " + newEmail + " est disponible.");
        }
    }

    /**
     * Vérifie si l'utilisateurId existe en base de données, s'il existe cela déclenche une exception.
     * @param utilisateurIdInAdherent
     * @param newUtilisateurId
     */
    public void checkIfUtilisateurIdExists(Long utilisateurIdInAdherent, Long newUtilisateurId) {
        if (utilisateurIdInAdherent == newUtilisateurId) {
            log.info("L'utilisateurId : " + newUtilisateurId + " existe déjà.");
            throw new FunctionalException("UtilisateurId déjà existant...");
        } else {
            log.info("L'utilisateurId : " + newUtilisateurId + " est disponible.");
        }
    }

    @Override
    public Adherent save(Adherent adherent, boolean update) {
        if (!update) {
            if (repository.findByEmail(adherent.getEmail()) != null) {
                Adherent adherentInDb = repository.findByEmail(adherent.getEmail());
                checkIfEmailExists(adherentInDb.getEmail(), adherent.getEmail());
            } else {
                checkIfEmailExists("", adherent.getEmail());
            }

            if (repository.findByUtilisateurId(adherent.getUtilisateurId()) != null) {
                Adherent adherentInDb = repository.findByUtilisateurId(adherent.getUtilisateurId());
                checkIfUtilisateurIdExists(adherentInDb.getUtilisateurId(), adherent.getUtilisateurId());
            } else {
                checkIfUtilisateurIdExists(Long.valueOf(0), adherent.getUtilisateurId());
            }
            log.info("L'enregistrement de l'adhérent " + adherent.getNom() + " " +
                    adherent.getPrenom() + " a bien été effectué.");
        } else {
            log.info("La modification de l'adhérent " + adherent.getNom() + " " +
                    adherent.getPrenom() + " a bien été effectué.");
        }

        return repository.save(adherent);
    }

    @Override
    public void delete(long id) {
        log.info("L'adhérent avec l'ID " + id + " a été supprimé.");
        repository.deleteById(id);
    }
}
