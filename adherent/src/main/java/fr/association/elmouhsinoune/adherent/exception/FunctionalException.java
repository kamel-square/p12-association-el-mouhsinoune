package fr.association.elmouhsinoune.adherent.exception;

public class FunctionalException extends RuntimeException {
    public FunctionalException(String message) {
        super(message);
    }
}
