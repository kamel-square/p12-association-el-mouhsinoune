package fr.association.elmouhsinoune.adherent.service.contract;

import fr.association.elmouhsinoune.adherent.entities.MembreFamille;

import java.util.List;

public interface MembreFamilleService {

    List<MembreFamille> findAll();
    List<MembreFamille> findByAdherentId(long adherentId);
    MembreFamille findById(long id);
    MembreFamille save(MembreFamille membreFamille);
    void delete(long id);
}
