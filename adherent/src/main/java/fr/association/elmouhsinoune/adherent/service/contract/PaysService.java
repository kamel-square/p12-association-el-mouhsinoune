package fr.association.elmouhsinoune.adherent.service.contract;

import fr.association.elmouhsinoune.adherent.entities.Pays;

import java.util.List;

public interface PaysService {

    List<Pays> findAll();
    Pays findById(long id);
    Pays save(Pays pays);
}
