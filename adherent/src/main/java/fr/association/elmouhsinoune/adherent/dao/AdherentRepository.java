package fr.association.elmouhsinoune.adherent.dao;

import fr.association.elmouhsinoune.adherent.entities.Adherent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RepositoryRestResource
public interface AdherentRepository extends JpaRepository<Adherent, Long> {

    @Query("select a from Adherent a where a.numAdherent like lower(:numAdherent) and lower(a.nom) like lower(:nom) and lower(a.prenom) like lower(:prenom)")
    Page<Adherent> search(@Param("numAdherent") String numAdherent, @Param("nom") String nom, @Param("prenom") String prenom, Pageable pageable);

    Adherent findByEmail(String email);

    @Query("select a from Adherent a where lower(a.nom) like lower(:nom)")
    List<Adherent> findByNom(@Param("nom") String nom);

    Adherent findByUtilisateurId(long utilisateurId);
}
