package fr.association.elmouhsinoune.adherent.service.impl;

import fr.association.elmouhsinoune.adherent.dao.PaysRepository;
import fr.association.elmouhsinoune.adherent.entities.Pays;
import fr.association.elmouhsinoune.adherent.service.contract.PaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaysServiceImpl implements PaysService {

    @Autowired
    private PaysRepository repository;

    @Override
    public List<Pays> findAll() {
        return repository.findAll();
    }

    @Override
    public Pays findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public Pays save(Pays pays) {
        return repository.save(pays);
    }
}
