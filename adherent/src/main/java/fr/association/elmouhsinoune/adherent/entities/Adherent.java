package fr.association.elmouhsinoune.adherent.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Adherent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String numAdherent;
    private String nom;
    private String prenom;
    @Email
    private String email;
    private String adresse;
    private String situation;
    private String telephone;
    @Column(name = "pays_id")
    private long paysId;
    @Column(name = "utilisateur_id")
    private long utilisateurId;
    @Column(name = "genre_id")
    private long genreId;

    @ManyToOne
    @JoinColumn(name = "genre_id", referencedColumnName = "id", insertable= false, updatable= false)
    private Genre genre;

    @ManyToOne
    @JoinColumn(name = "pays_id", referencedColumnName = "id", insertable= false, updatable= false)
    private Pays pays;

}
