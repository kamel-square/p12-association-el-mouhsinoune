package fr.association.elmouhsinoune.adherent.service.impl;

import fr.association.elmouhsinoune.adherent.dao.MembreFamilleRepository;
import fr.association.elmouhsinoune.adherent.entities.MembreFamille;
import fr.association.elmouhsinoune.adherent.service.contract.MembreFamilleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MembreFamilleServiceImpl implements MembreFamilleService {

    @Autowired
    private MembreFamilleRepository repository;

    Logger log = LoggerFactory.getLogger(MembreFamilleServiceImpl.class);

    @Override
    public List<MembreFamille> findAll() {
        return repository.findAll();
    }

    @Override
    public List<MembreFamille> findByAdherentId(long adherentId) {
        return repository.findByAdherentId(adherentId);
    }

    @Override
    public MembreFamille findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public MembreFamille save(MembreFamille membreFamille) {
        return repository.save(membreFamille);
    }

    @Override
    public void delete(long id) {
        log.info("Le membre de la famille avec l'ID " + id + " a été supprimé.");
        repository.deleteById(id);
    }
}
