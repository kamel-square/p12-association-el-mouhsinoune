package fr.association.elmouhsinoune.adherent.controller;

import fr.association.elmouhsinoune.adherent.entities.Pays;
import fr.association.elmouhsinoune.adherent.service.contract.PaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PaysController {

    @Autowired
    private PaysService service;

    /**
     * Affiche la liste des pays.
     */
    @GetMapping(value = "/pays")
    public List<Pays> paysList() {
        return service.findAll();
    }

    /**
     * Affiche un pays de par son id.
     */
    @GetMapping(value = "/pays/{id}")
    public Pays getPaysById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Ajoute un pays.
     */
    @PostMapping(value = "/addPays")
    public Pays addPays(@RequestBody Pays pays) {
        return service.save(pays);
    }
}
