package fr.association.elmouhsinoune.adherent.controller;

import fr.association.elmouhsinoune.adherent.entities.Adherent;
import fr.association.elmouhsinoune.adherent.service.contract.AdherentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AdherentController {

    @Autowired
    private AdherentService service;

    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Affiche la liste des adhérents.
     */
    @GetMapping(value = "/adherents")
    public List<Adherent> adherentList() {
        return service.findAll();
    }

    /**
     * Affiche un adhérent de par son id.
     */
    @GetMapping(value = "/adherent/{id}")
    public Adherent getAdherentById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Affiche le résultat de la recherche.
     * @param numAdherent
     * @param nom
     * @param prenom
     * @param page
     * @param size
     */
    @GetMapping(value = "/searchAdherent")
    public Page<Adherent> searchAdherent(@RequestParam(name = "numAdherent", defaultValue = "") String numAdherent,
                                         @RequestParam(name = "nom", defaultValue = "") String nom,
                                         @RequestParam(name = "prenom", defaultValue = "") String prenom,
                                         @RequestParam(name = "page", defaultValue = "0") int page,
                                         @RequestParam(name = "size", defaultValue = "10") int size) {

        if (!nom.equals("")) {
            log.info("Nom de l'adhérent : " + nom);
        }
        return service.search("%" + numAdherent + "%", "%" + nom + "%", "%" + prenom + "%", page, size);
    }

    /**
     * Affiche le résultat de la recherche.
     * @param nom
     */
    @GetMapping(value = "/getAdherentByNom")
    public List<Adherent> getAdherentByNom(@RequestParam(name = "nom", defaultValue = "") String nom) {
        return service.findByNom("%" + nom + "%");
    }

    /**
     * Affiche un adhérent de par l'utilisateurId.
     */
    @GetMapping(value = "/getAdherentByUtilisateurId/{utilisateurId}")
    public Adherent getAdherentByUtilisateurId(@PathVariable("utilisateurId") long utilisateurId) {
        return service.findByUtilisateurId(utilisateurId);
    }

    /**
     * Ajoute un adhérent.
     */
    @PostMapping(value = "/saveAdherent")
    public Adherent saveAdherent(@RequestBody Adherent adherent) {
        return service.save(adherent, false);
    }

    /**
     * Modifie un adhérent.
     */
    @PutMapping(value = "/updateAdherent")
    public Adherent updateAdherent(@RequestBody Adherent adherent) {
        return service.save(adherent, true);
    }

    /**
     * Supprime un adhérent.
     */
    @DeleteMapping(value = "/deleteAdherent/{id}")
    public void deleteAdherent(@PathVariable("id") long id) {
        service.delete(id);
    }
}
