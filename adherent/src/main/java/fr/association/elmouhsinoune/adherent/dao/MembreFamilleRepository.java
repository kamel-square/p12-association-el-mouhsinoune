package fr.association.elmouhsinoune.adherent.dao;

import fr.association.elmouhsinoune.adherent.entities.MembreFamille;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface MembreFamilleRepository extends JpaRepository<MembreFamille, Long> {

    List<MembreFamille> findByAdherentId(long adherentId);

}
