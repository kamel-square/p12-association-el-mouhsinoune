package fr.association.elmouhsinoune.adherent.service.impl;

import fr.association.elmouhsinoune.adherent.dao.LienParenteRepository;
import fr.association.elmouhsinoune.adherent.entities.LienParente;
import fr.association.elmouhsinoune.adherent.service.contract.LienParenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LienParenteServiceImpl implements LienParenteService {

    @Autowired
    private LienParenteRepository repository;

    @Override
    public List<LienParente> findAll() {
        return repository.findAll();
    }

    @Override
    public LienParente findById(long id) {
        return repository.findById(id).get();
    }

    @Override
    public LienParente save(LienParente lienParente) {
        return repository.save(lienParente);
    }
}
