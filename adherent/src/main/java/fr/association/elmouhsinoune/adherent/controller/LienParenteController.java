package fr.association.elmouhsinoune.adherent.controller;

import fr.association.elmouhsinoune.adherent.entities.LienParente;
import fr.association.elmouhsinoune.adherent.service.contract.LienParenteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class LienParenteController {

    @Autowired
    private LienParenteService service;

    /**
     * Affiche la liste des liens de parenté.
     */
    @GetMapping(value = "/lienParentes")
    public List<LienParente> lienParentes() {
        return service.findAll();
    }

    /**
     * Affiche un lien de parenté de par son id.
     */
    @GetMapping(value = "/lienParentes/{id}")
    public LienParente lienParentesById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Ajoute un lien de parenté.
     */
    @PostMapping(value = "/saveLienParente")
    public LienParente saveLienParente(@RequestBody LienParente lienParente) {
        return service.save(lienParente);
    }
}
