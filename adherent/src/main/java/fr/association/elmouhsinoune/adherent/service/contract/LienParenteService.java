package fr.association.elmouhsinoune.adherent.service.contract;

import fr.association.elmouhsinoune.adherent.entities.LienParente;

import java.util.List;

public interface LienParenteService {

    List<LienParente> findAll();
    LienParente findById(long id);
    LienParente save(LienParente lienParente);
}
