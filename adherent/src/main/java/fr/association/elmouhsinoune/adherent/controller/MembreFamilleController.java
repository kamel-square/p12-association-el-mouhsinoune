package fr.association.elmouhsinoune.adherent.controller;

import fr.association.elmouhsinoune.adherent.entities.MembreFamille;
import fr.association.elmouhsinoune.adherent.service.contract.MembreFamilleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MembreFamilleController {

    @Autowired
    private MembreFamilleService service;

    /**
     * Affiche la liste des membres de famille.
     */
    @GetMapping(value = "/membreFamilles")
    public List<MembreFamille> membreFamilleList() {
        return service.findAll();
    }

    /**
     * Affiche un membre de famille de par son id.
     */
    @GetMapping(value = "/membreFamilles/{id}")
    public MembreFamille getMembreFamilleById(@PathVariable("id") long id) {
        return service.findById(id);
    }

    /**
     * Affiche la liste des membres de famille de par l'adherentId.
     */
    @GetMapping(value = "/getMembreFamillesByAdherentId/{adherentId}")
    public List<MembreFamille> getMembreFamillesByAdherentId(@PathVariable("adherentId") long adherentId) {
        return service.findByAdherentId(adherentId);
    }

    /**
     * Ajoute un membre de famille.
     */
    @PostMapping(value = "/saveMembre")
    public MembreFamille saveMembre(@RequestBody MembreFamille membreFamille) {
        return service.save(membreFamille);
    }

    /**
     * Modifie un membre de famille.
     */
    @PutMapping(value = "/updateMembre")
    public MembreFamille updateMembre(@RequestBody MembreFamille membreFamille) {
        return service.save(membreFamille);
    }

    /**
     * Supprime un membre de famille.
     */
    @DeleteMapping(value = "/deleteMembreFamille/{id}")
    public void deleteMembreFamille(@PathVariable("id") long id) {
        service.delete(id);
    }
}
