TRUNCATE TABLE genre CASCADE;
INSERT INTO genre (id, genre)
VALUES
(1, 'Homme'),
(2, 'Femme');

TRUNCATE TABLE lien_parente CASCADE;
INSERT INTO lien_parente (id, lien)
VALUES
(1, 'Épouse'),
(2, 'Époux'),
(3, 'Fils'),
(4, 'Fille');

TRUNCATE TABLE pays CASCADE;
INSERT INTO pays (id, pays)
VALUES
(1, 'France'),
(2, 'Algérie'),
(3, 'Maroc'),
(4, 'Tunisie'),
(5, 'Mali'),
(6, 'Sénégal'),
(7, 'Cameroun'),
(8, 'Côte d''Ivoire'),
(9, 'Comores'),
(10, 'Mauritanie');

TRUNCATE TABLE adherent CASCADE;
INSERT INTO adherent (id, genre_id, num_adherent, nom, prenom, adresse, email, situation, telephone, pays_id, utilisateur_id)
VALUES
(1, 1, '40125', 'Hauchard', 'Eric', '10 Avenue Rouget de Lisle, 94400 Vitry-sur-Seine', 'eric.hauchard@gmail.com', 'Célibataire', '06 45 02 54 12', 1, 1),
(2, 1, '42569', 'Selhemi', 'Ahmed', '7 Rue de la Glacière, 94400 Vitry-sur-Seine', 'ahmed.selhemi@gmail.com', 'Marié', '06 85 48 25 63', 2, 2),
(3, 1, '48965', 'Kante', 'Moussa', '1 Rue Guy Môquet, 94600 Choisy-le-Roi', 'moussa.kante@gmail.com', 'Marié', '07 54 02 65 11', 5, 3),
(4, 2, '25844', 'Bedia', 'Nawel', '39 Rue René Hamon, 94800 Villejuif', 'nawel.bedia@gmail.com', 'Divorcée', '06 51 25 36 97', 1, 4);

TRUNCATE TABLE membre_famille CASCADE;
INSERT INTO membre_famille (id, nom, prenom, adherent_id, lien_parente_id)
VALUES
(1, 'Selhemi', 'Issa', 2, 3),
(2, 'Selhemi', 'Malika', 2, 4),
(3, 'Kante', 'Fatou', 3, 1),
(4, 'Kante', 'Assa', 3, 4),
(5, 'Kante', 'Mamadou', 3, 3),
(6, 'Bedia', 'Ismaïl', 4, 3);
