DROP SEQUENCE IF EXISTS public.genre_id_seq CASCADE;
CREATE SEQUENCE public.genre_id_seq;

DROP TABLE IF EXISTS public.genre CASCADE;
CREATE TABLE public.genre (
    id BIGINT NOT NULL DEFAULT nextval('public.genre_id_seq'),
    genre VARCHAR(5) NOT NULL,
    CONSTRAINT genre_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.genre_id_seq OWNED BY public.genre.id;

DROP SEQUENCE IF EXISTS public.lien_parente_id_seq CASCADE;
CREATE SEQUENCE public.lien_parente_id_seq;

DROP TABLE IF EXISTS public.lien_parente CASCADE;
CREATE TABLE public.lien_parente (
                id BIGINT NOT NULL DEFAULT nextval('public.lien_parente_id_seq'),
                lien VARCHAR(10) NOT NULL,
                CONSTRAINT lien_parente_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.lien_parente_id_seq OWNED BY public.lien_parente.id;

DROP SEQUENCE IF EXISTS public.pays_id_seq CASCADE;
CREATE SEQUENCE public.pays_id_seq;

DROP TABLE IF EXISTS public.pays CASCADE;
CREATE TABLE public.pays (
                id BIGINT NOT NULL DEFAULT nextval('public.pays_id_seq'),
                pays VARCHAR NOT NULL,
                CONSTRAINT pays_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.pays_id_seq OWNED BY public.pays.id;

DROP SEQUENCE IF EXISTS public.adherent_id_seq CASCADE;
CREATE SEQUENCE public.adherent_id_seq;

DROP TABLE IF EXISTS public.adherent CASCADE;
CREATE TABLE public.adherent (
                id BIGINT NOT NULL DEFAULT nextval('public.adherent_id_seq'),
                genre_id BIGINT NOT NULL,
                num_adherent VARCHAR(6) UNIQUE NOT NULL,
                nom VARCHAR(30) NOT NULL,
                prenom VARCHAR(30) NOT NULL,
                adresse VARCHAR NOT NULL,
                email VARCHAR(50) UNIQUE NOT NULL,
                situation VARCHAR(15) NOT NULL,
                telephone VARCHAR(15),
                pays_id BIGINT NOT NULL,
				utilisateur_id BIGINT NOT NULL,
                CONSTRAINT adherent_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.adherent_id_seq OWNED BY public.adherent.id;

DROP SEQUENCE IF EXISTS public.membre_famille_id_seq CASCADE;
CREATE SEQUENCE public.membre_famille_id_seq;

DROP TABLE IF EXISTS public.membre_famille CASCADE;
CREATE TABLE public.membre_famille (
                id BIGINT NOT NULL DEFAULT nextval('public.membre_famille_id_seq'),
                nom VARCHAR(30) NOT NULL,
                prenom VARCHAR(30) NOT NULL,
                adherent_id BIGINT NOT NULL,
                lien_parente_id BIGINT NOT NULL,
                CONSTRAINT membre_famille_pk PRIMARY KEY (id)
);

ALTER SEQUENCE public.membre_famille_id_seq OWNED BY public.membre_famille.id;


ALTER TABLE public.membre_famille ADD CONSTRAINT lien_parente_membre_famille_fk
FOREIGN KEY (lien_parente_id)
REFERENCES public.lien_parente (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.adherent ADD CONSTRAINT pays_adherent_fk
FOREIGN KEY (pays_id)
REFERENCES public.pays (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.membre_famille ADD CONSTRAINT adherent_membre_famille_fk
FOREIGN KEY (adherent_id)
REFERENCES public.adherent (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.adherent ADD CONSTRAINT genre_adherent_fk
FOREIGN KEY (genre_id)
REFERENCES public.genre (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
